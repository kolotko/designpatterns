﻿using Prototype.CustomExceptions;
using Prototype.Enums;

namespace Prototype.Abstraction
{
    public abstract class Animal
    {
        public string Name { get; set; }
        public abstract AnimalBreed breed { get; }

        /// <summary>
        /// Get the sound of an animal
        /// </summary>
        /// <returns></returns>
        public abstract string Sound();

        /// <summary>
        /// Get a description about the animal
        /// </summary>
        /// <returns></returns>
        public string Description()
        {
            return $"{translateName()} o imieniu: {Name}";
        }

        /// <summary>
        /// Get a Polish breed translation
        /// </summary>
        /// <returns></returns>
        /// <exception cref="BreedNotSetException">An exception when an animal has no established breed</exception>
        public string translateName()
        {
            switch (this.breed)
            {
                case AnimalBreed.Dog:
                    return "Pies";
                case AnimalBreed.Cat:
                    return "Kot";
                case AnimalBreed.Cow:
                    return "Krowa";
                case AnimalBreed.Pig:
                    return "Świnia";
            }

            throw new BreedNotSetException("Nie ustawiono rasy zwierzęcia");
        }
    }
}
