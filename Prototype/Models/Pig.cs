﻿using Prototype.Abstraction;
using Prototype.Enums;

namespace Prototype.Models
{
    public class Pig : Animal, ICloneable
    {
        private AnimalBreed _breed = AnimalBreed.Pig;
        public override AnimalBreed breed { get => _breed; }

        public object Clone()
        {
            return this.MemberwiseClone();
        }

        /// <summary>
        /// Get the sound of an animal
        /// </summary>
        /// <returns></returns>
        public override string Sound()
        {
            return "chrum chrum";
        }
    }
}
