﻿using Prototype.Abstraction;
using Prototype.Enums;

namespace Prototype.Models
{
    public class Dog : Animal, ICloneable
    {
        private AnimalBreed _breed = AnimalBreed.Dog;
        public override AnimalBreed breed { get => _breed; }

        public object Clone()
        {
            return this.MemberwiseClone();
        }

        /// <summary>
        /// Get the sound of an animal
        /// </summary>
        /// <returns></returns>
        public override string Sound()
        {
            return "hał hał";
        }
    }
}
