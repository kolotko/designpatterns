﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Prototype;
using Prototype.Models;

var services = ConfigureServices();
var servicesProvider = services.BuildServiceProvider();
servicesProvider.GetService<Application>()?.Run();

/// <summary>
/// Register services
/// </summary>
static IServiceCollection ConfigureServices()
{
    IServiceCollection services = new ServiceCollection();

    var config = LoadConfiguration();
    services.AddSingleton(config);

    services.AddSingleton(new Dog()
    {
        Name = "This dog has no name yet."
    });

    services.AddSingleton(new Cat()
    {
        Name = "This cat has no name yet."
    });

    services.AddSingleton(new Pig()
    {
        Name = "This pig has no name yet."
    });

    services.AddSingleton(new Cow()
    {
        Name = "This cow has no name yet."
    });

    services.AddSingleton<Application>();

    return services;
}

/// <summary>
/// Get configuration from file
/// </summary>
static IConfiguration LoadConfiguration()
{
    var builder = new ConfigurationBuilder()
            .SetBasePath(Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);

    return builder.Build();
}