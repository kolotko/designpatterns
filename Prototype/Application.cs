﻿using Prototype.Abstraction;
using Prototype.CustomExceptions;
using Prototype.Models;

namespace Prototype
{
    public class Application
    {
        private readonly Dog _dog;
        private readonly Cat _cat;
        private readonly Pig _pig;
        private readonly Cow _cow;
        private List<Animal> _myAnimal;

        public Application(Dog dog, Cat cat, Pig pig, Cow cow)
        {
            _dog = dog;
            _cat = cat;
            _pig = pig;
            _cow = cow;
            _myAnimal = new List<Animal>();
        }
        public void Run()
        {
            Console.WriteLine(@"
                    Witaj
            ");
            while(true)
            {
                Console.WriteLine(@"
                    Jesteśmy profesjonalnym sklepem dla zwierząt. W czym możemy pomóc?
                    1: kup zwierzę
                    2: Wyświetl kupione zwierzęta                
                ");

                string userInput = Console.ReadLine() ?? "";
                if (String.IsNullOrEmpty(userInput) || !new[] { "1", "2", "3" }.Contains(userInput))
                {
                    Console.Clear();
                    Console.WriteLine(@"
                    Wprowadzono niepoprawną wartość, spróbuj jeszcze raz.
                    ");
                    continue;
                }

                switch (userInput)
                {
                    case "1":
                        AddAnimal();
                        break;
                    case "2":
                        DisplayMyAnimals();
                        break;
                    case"3":
                        break;
                }
            }
        }
        /// <summary>
        /// Add new animal to list
        /// </summary>
        private void AddAnimal()
        {
            Console.Clear();
            while (true)
            {
                Console.WriteLine(@"
                    Jakie zwierzę chciał byś kupić? 
                    1: pies
                    2: kot
                    3: krowa
                    4: świnia
                    5: powrót
                ");

                string userInput = Console.ReadLine() ?? "";
                if (String.IsNullOrEmpty(userInput) || !new[] { "1", "2", "3", "4", "5" }.Contains(userInput))
                {
                    Console.Clear();
                    Console.WriteLine(@"
                    Wprowadzono niepoprawną wartość, spróbuj jeszcze raz.
                    ");
                }

                switch (userInput)
                {
                    case "1":
                        _myAnimal.Add(_dog.Clone() as Animal);
                        SetAnimalName(_myAnimal.Count - 1);
                        return;
                    case "2":
                        _myAnimal.Add(_cat.Clone() as Animal);
                        SetAnimalName(_myAnimal.Count - 1);
                        return;
                    case "3":
                        _myAnimal.Add(_cow.Clone() as Animal);
                        SetAnimalName(_myAnimal.Count - 1);
                        return;
                    case "4":
                        _myAnimal.Add(_pig.Clone() as Animal);
                        SetAnimalName(_myAnimal.Count - 1);
                        return;
                    case "5":
                        Console.Clear();
                        return;
                }
            }
        }

        /// <summary>
        /// Set name for animal with selected index
        /// </summary>
        /// <param name="animalIndex">Index of animal on list</param>
        private void SetAnimalName(int animalIndex)
        {
            Console.Clear();
            Console.WriteLine(@"
                Wpisz imię dla nowego pupila"
            );
            string userInput = Console.ReadLine() ?? "";
            _myAnimal[animalIndex].Name = userInput;
            Console.Clear();
        }

        /// <summary>
        /// Display breed and name of animal 
        /// </summary>
        private void DisplayMyAnimals()
        {
            Console.Clear();
            if (_myAnimal.Count == 0)
            {
                Console.WriteLine(@"
                    Nie posiadasz żadnych zwierząt.
                ");
                return;
            }

            foreach (var animal in _myAnimal)
            {
                try
                {
                    Console.WriteLine($@"
                        {animal.Description()}
                    ");
                }
                catch (BreedNotSetException)
                {
                    Console.Clear();
                    Console.WriteLine($@"
                        Ups coś poszło nie tak.
                    ");
                }
                
            }

            Console.WriteLine(@"
                        Naciśnij dowolny klawisz by wrócić
            ");
            string userInput = Console.ReadLine() ?? "";
            Console.Clear();
            return;
        }
    }
}
