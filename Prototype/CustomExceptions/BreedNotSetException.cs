﻿namespace Prototype.CustomExceptions
{
    public class BreedNotSetException : Exception
    {
        public BreedNotSetException(string message) : base(message) { }
    }
}
