﻿using Facade.Class;
using Facade.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Facade
{
    public class Application
    {
        private IStockStatus _stockStatus { get; set; }
        private IOrderProduct _orderProduct { get; set; }
        private ISendNotification _sendNotification { get; set; }
        private FacadeImplementation _facadeImplementation { get; set; }

        public Application(IStockStatus stockStatus, IOrderProduct orderProduct, ISendNotification sendNotification)
        {
            _stockStatus = stockStatus;
            _orderProduct = orderProduct;
            _sendNotification = sendNotification;
            _facadeImplementation = new FacadeImplementation(_stockStatus, _orderProduct, _sendNotification);

        }
        public void Run()
        {
            

            while (true)
            {
                Console.WriteLine($"W naszym magazynie znajduje się {_stockStatus.GetProductCount()} produktów");
                Console.WriteLine("");
                Console.WriteLine("Aby złożyć zamówienie naciśnij dowolny klawisz");
                Console.ReadLine();
                Console.Clear();
                _facadeImplementation.MakeOrder();
                Console.WriteLine("Aby kontynuować naciśnij dowolny klawisz");
                Console.ReadLine();
                Console.Clear();

            }
        }
    }
}
