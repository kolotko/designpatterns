﻿using Facade.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Facade.Class
{
    public class SendNotification : ISendNotification
    {
        public void Send(bool isOrderCorrect)
        {
            if (isOrderCorrect)
                Console.WriteLine("Powiadomienie SMS: Pomyślnie zakupiono produkt.");
            else
                Console.WriteLine("Przykro nam ale magazyn jest pusty");

        }
    }
}
