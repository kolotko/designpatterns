﻿using Facade.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Facade.Class
{
    public class FacadeImplementation
    {
        public IStockStatus _stockStatus { get; set; }
        public IOrderProduct _orderProduct { get; set; }
        public ISendNotification _sendNotification { get; set; }
        public FacadeImplementation(IStockStatus stockStatus, IOrderProduct orderProduct, ISendNotification sendNotification)
        {
            _stockStatus = stockStatus;
            _orderProduct = orderProduct;
            _sendNotification = sendNotification;
        }

        public void MakeOrder()
        {
            var productCount = _stockStatus.GetProductCount();
            var isOrderCorrect = _orderProduct.TryOrderProduct(productCount);
            if (isOrderCorrect)
                _stockStatus.SubtractProduct();
            _sendNotification.Send(isOrderCorrect);
        }
    }
}
