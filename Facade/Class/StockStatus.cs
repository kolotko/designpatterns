﻿using Facade.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Facade.Class
{
    public class StockStatus : IStockStatus
    {
        private int ProductCount = 5;

        public int GetProductCount()
        {
            return ProductCount;
        }

        public void SubtractProduct()
        {
            ProductCount--;
        }
    }
}
