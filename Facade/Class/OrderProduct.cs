﻿using Facade.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Facade.Class
{
    public class OrderProduct : IOrderProduct
    {
        public bool TryOrderProduct(int productCount)
        {
            if (productCount > 0)
                return true;
            return false;
        }
    }
}
