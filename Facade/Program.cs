﻿using Facade;
using Facade.Class;
using Facade.Interfaces;
using Microsoft.Extensions.DependencyInjection;

var services = ConfigureServices();
var servicesProvider = services.BuildServiceProvider();
servicesProvider.GetService<Application>()?.Run();

/// <summary>
/// Register services
/// </summary>
static IServiceCollection ConfigureServices()
{
    IServiceCollection services = new ServiceCollection();

    services.AddSingleton<IStockStatus>(new StockStatus());
    services.AddSingleton<IOrderProduct>(new OrderProduct());
    services.AddSingleton<ISendNotification>(new SendNotification());
    services.AddSingleton<Application>();

    return services;
}