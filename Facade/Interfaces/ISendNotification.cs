﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Facade.Interfaces
{
    public interface ISendNotification
    {
        public void Send(bool isStoreEmpty);
    }
}
