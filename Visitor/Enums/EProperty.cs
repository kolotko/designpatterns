﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Visitor.Enums
{
    internal enum EProperty
    {
        House = 1,
        Gallery = 2,
        Factory = 3
    }
}
