﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Visitor.Class;

namespace Visitor.Interfaces
{
    internal interface IInsurance
    {
        void HouseInsurance(House house);
        void GalleryInsurance(Gallery gallery);
        void FactoryInsurance(Factory factory);
    }
}
