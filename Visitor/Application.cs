﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Visitor.Class;
using Visitor.Enums;
using Visitor.Interfaces;

namespace Visitor
{
    internal class Application
    {
        private readonly IInsurance _insurance;
        private readonly Factory _factory;
        private readonly Gallery _gallery;
        private readonly House _house;

        public Application(IInsurance insurance, Factory factory, Gallery gallery, House house)
        {
            _insurance = insurance;
            _factory = factory;
            _gallery = gallery;
            _house = house;
        }

        public void Run()
        {
            Console.WriteLine("Witaj w naszej firmie ubezpieczeniowej.");
            while (true)
            {
                Console.WriteLine("Co możemy dla Ciebie ubezpieczyć?");
                Console.WriteLine("1: Dom");
                Console.WriteLine("2: Galeria");
                Console.WriteLine("3: Fabryka");

                var userInput = Console.ReadLine() ?? "";

                if (!ValidateUserInput(userInput, new[] { "1", "2", "3" }))
                    continue;

                Console.Clear();
                EProperty property = (EProperty)Enum.Parse(typeof(EProperty), userInput);

                switch (property)
                {
                    case EProperty.House:
                        _house.Accept(_insurance);
                        break;
                    case EProperty.Gallery:
                        _gallery.Accept(_insurance);
                        break;
                    case EProperty.Factory:
                        _factory.Accept(_insurance);
                        break;
                }

                Console.ReadLine();
                Console.Clear();
            }
        }

        private bool ValidateUserInput(string userInput, string[] validValues)
        {
            if (String.IsNullOrEmpty(userInput) || !validValues.Contains(userInput))
            {
                Console.Clear();
                Console.WriteLine("Wprowadzono niepoprawną wartość, spróbuj jeszcze raz.");
                return false;
            }
            return true;
        }
    }
}
