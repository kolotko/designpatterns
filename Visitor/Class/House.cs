﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Visitor.Interfaces;

namespace Visitor.Class
{
    internal class House : IProperty
    {
        public void Accept(IInsurance insurance)
        {
            insurance.HouseInsurance(this);
        }
    }
}
