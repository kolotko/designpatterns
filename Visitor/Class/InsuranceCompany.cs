﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Visitor.Interfaces;

namespace Visitor.Class
{
    internal class InsuranceCompany : IInsurance
    {
        public void FactoryInsurance(Factory factory)
        {
            Console.WriteLine("Ubezpieczenie dla fabryki wynosi 1000zł miesięcznie.");
        }

        public void GalleryInsurance(Gallery gallery)
        {
            Console.WriteLine("Ubezpieczenie dla galeri wynosi 700zł miesięcznie.");
        }

        public void HouseInsurance(House house)
        {
            Console.WriteLine("Ubezpieczenie dla domu wynosi 500zł miesięcznie.");
        }
    }
}
