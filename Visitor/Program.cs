﻿using Microsoft.Extensions.DependencyInjection;
using Visitor;
using Visitor.Class;
using Visitor.Interfaces;

var services = ConfigureServices();
var servicesProvider = services.BuildServiceProvider();
servicesProvider.GetService<Application>()?.Run();

/// <summary>
/// Register services
/// </summary>
static IServiceCollection ConfigureServices()
{
    IServiceCollection services = new ServiceCollection();

    services.AddSingleton<Application>();
    services.AddSingleton<IInsurance>(new InsuranceCompany());
    services.AddSingleton<Factory>();
    services.AddSingleton<Gallery>();
    services.AddSingleton<House>();

    return services;
}