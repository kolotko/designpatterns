﻿using Strategy.Class;
using Strategy.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strategy
{
    internal class Application
    {
        private readonly DisplayString _displayString;
        private string _userInput;
        private string _userSelectedMethod;
        public Application(DisplayString displayString)
        {
            _displayString = displayString;
            _userInput = "";
            _userSelectedMethod = "";
        }
        public void Run()
        {
            Console.WriteLine("Witaj");
            while (true)
            {
                GetUserValue();
                SelectMethod();

                if (!ValidateUserInput(_userSelectedMethod, new[] { "1", "2", "3", "4" }))
                    continue;

                PrepareObjectAndDisplay(_userSelectedMethod, _userInput);

                Console.ReadLine();
                _userInput = "";
                Console.Clear();
            }
        }

        private void GetUserValue()
        {
            if (!string.IsNullOrEmpty(_userInput))
                return;
            Console.WriteLine("Wprowadź dowolny ciąg znaków");

            _userInput = Console.ReadLine() ?? "";

            Console.Clear();
        }

        private void SelectMethod()
        {
            Console.WriteLine("Wybierz sposób w jaki mamy wyświetlić podane dane:");
            Console.WriteLine("1: bez modyfikacji");
            Console.WriteLine("2: w odwróconej kolejności");
            Console.WriteLine("3: oddzielone litery");
            Console.WriteLine("4: litery w nowej lini");

            _userSelectedMethod = Console.ReadLine() ?? "";
        }

        private void PrepareObjectAndDisplay(string userSelectedMethod, string userInput)
        {
            var _eDisplayString = (EDisplayString)Enum.Parse(typeof(EDisplayString), userSelectedMethod);

            switch (_eDisplayString)
            {
                case EDisplayString.NoModify:
                    _displayString.SetStrategy(new NoModify());
                    break;
                case EDisplayString.RevertString:
                    _displayString.SetStrategy(new RevertString());
                    break;
                case EDisplayString.SeparateLetters:
                    _displayString.SetStrategy(new SeparateLetters());
                    break;
                case EDisplayString.LetterInNewLine:
                    _displayString.SetStrategy(new LetterInNewLine());
                    break;
                default:
                    _displayString.SetStrategy(new NoModify());
                    break;
            }

            _displayString.Display(userInput);
        }

        private bool ValidateUserInput(string userInput, string[] validValues)
        {
            if (String.IsNullOrEmpty(userInput) || !validValues.Contains(userInput))
            {
                Console.Clear();
                Console.WriteLine("Wprowadzono niepoprawną wartość, spróbuj jeszcze raz.");
                return false;
            }
            return true;
        }
    }
}
