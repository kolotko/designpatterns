﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strategy.Enums
{
    internal enum EDisplayString
    {
        NoModify = 1,
        RevertString = 2,
        SeparateLetters = 3,
        LetterInNewLine = 4,
    }
}
