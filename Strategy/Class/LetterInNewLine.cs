﻿using Strategy.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strategy.Class
{
    internal class LetterInNewLine : IDisplayString
    {
        public void Display(string value)
        {
            foreach (var letter in value)
                Console.WriteLine(letter);
        }
    }
}
