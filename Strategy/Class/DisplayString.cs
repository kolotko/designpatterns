﻿using Strategy.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strategy.Class
{
    internal class DisplayString
    {
        private IDisplayString _displayString;

        public void SetStrategy(IDisplayString strategy)
        {
            _displayString = strategy;
        }

        public void Display(string value)
        {
            Console.Clear();
            _displayString.Display(value);
        }
    }
}
