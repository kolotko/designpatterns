﻿using Strategy.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Strategy.Class
{
    internal class SeparateLetters : IDisplayString
    {
        public void Display(string value)
        {
            StringBuilder sb = new StringBuilder();
            foreach (var item in value)
            {
                sb.Append(item + " ");
            }

            Console.WriteLine(sb.ToString());
        }
    }
}
