﻿using Microsoft.Extensions.DependencyInjection;
using Strategy;
using Strategy.Class;

var services = ConfigureServices();
var servicesProvider = services.BuildServiceProvider();
servicesProvider.GetService<Application>()?.Run();

static IServiceCollection ConfigureServices()
{
    IServiceCollection services = new ServiceCollection();
    services.AddSingleton<Application>();
    services.AddSingleton<DisplayString>();

    return services;
}
