﻿using ChainOfResponsibility.Enums;
using ChainOfResponsibility.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChainOfResponsibility.Class
{
    internal class Administrator : ISupport
    {
        private ISupport _support;
        public void CheckProblem(EProblem problem)
        {
            if (EProblem.AddUser == problem ||
                EProblem.GrantPermission == problem)
            {
                SolveProblem(problem);
            }
            else
            {
                _support.CheckProblem(problem);
            }
        }

        public void SetSupervisor(ISupport support)
        {
            _support = support;
        }

        public void SolveProblem(EProblem problem)
        {
            Console.WriteLine("Twój problem został zaadresowany do działu administracyjnego.");
            switch (problem)
            {
                case EProblem.AddUser:
                    Console.WriteLine("Dodano nowego użytkownika do systemu. Na twój mail przyjdą dane logowania dla nowego konta.");
                    break;
                case EProblem.GrantPermission:
                    Console.WriteLine("Przyznano nowe uprawnienie dla Twojego konta.");
                    break;
                default:
                    throw new Exception("Niestety nie możemy rozwiązać Twojego problemu.");
            }
        }
    }
}
