﻿using ChainOfResponsibility.Enums;
using ChainOfResponsibility.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChainOfResponsibility.Class
{
    internal class CallCenter : ISupport
    {
        private ISupport _support;
        public void CheckProblem(EProblem problem)
        {
            if (EProblem.ForgotPassword == problem ||
                EProblem.LostMail == problem)
            {
                SolveProblem(problem);
            }
            else
            {
                _support.CheckProblem(problem);
            }
        }

        public void SetSupervisor(ISupport support)
        {
            _support = support;
        }

        public void SolveProblem(EProblem problem)
        {
            Console.WriteLine("Twój problem został zaadresowany do działu wsparcia.");
            switch (problem)
            {
                case EProblem.ForgotPassword:
                    Console.WriteLine("Na Twojego maila zostało wysłane zastępcze hasło.");
                    break;
                case EProblem.LostMail:
                    Console.WriteLine("Na Twój email został wysłany ostatnio zaginiony mail.");
                    break;
                default:
                    throw new Exception("Niestety nie możemy rozwiązać Twojego problemu.");
            }
        }
    }
}
