﻿using ChainOfResponsibility.Enums;
using ChainOfResponsibility.Interfaces;

namespace ChainOfResponsibility.Class
{
    internal class SoftwareDeveloper : ISupport
    {
        private ISupport _support;
        public void CheckProblem(EProblem problem)
        {
            if (EProblem.ServerNotRespond == problem)
            {
                SolveProblem(problem);
            }
            else
            {
                throw new Exception("Niestety nie możemy rozwiązać Twojego problemu.");
            }
        }

        public void SetSupervisor(ISupport support)
        {
            _support = support;
        }

        public void SolveProblem(EProblem problem)
        {
            Console.WriteLine("Twój problem został zaadresowany do działu programistów.");
            switch (problem)
            {
                case EProblem.ServerNotRespond:
                    Console.WriteLine("Reset serwera pomógł przywrócić go do pełni sprawności. Dziękujemy za zgłoszenie.");
                    break;
                default:
                    throw new Exception("Niestety nie możemy rozwiązać Twojego problemu.");
            }
        }
    }
}
