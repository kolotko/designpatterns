﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChainOfResponsibility.Enums
{
    internal enum EProblem
    {
        ForgotPassword = 1,
        LostMail = 2,
        AddUser = 3,
        GrantPermission = 4,
        ServerNotRespond = 5,
    }
}
