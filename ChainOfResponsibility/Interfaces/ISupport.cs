﻿using ChainOfResponsibility.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChainOfResponsibility.Interfaces
{
    internal interface ISupport
    {
        void CheckProblem(EProblem problem);
        void SolveProblem(EProblem problem);
        void SetSupervisor(ISupport support);
    }
}
