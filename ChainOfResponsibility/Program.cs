﻿using ChainOfResponsibility;
using ChainOfResponsibility.Class;
using Microsoft.Extensions.DependencyInjection;

var services = ConfigureServices();
var servicesProvider = services.BuildServiceProvider();
servicesProvider.GetService<Application>()?.Run();

static IServiceCollection ConfigureServices()
{
    IServiceCollection services = new ServiceCollection();
    services.AddSingleton<Application>();
    services.AddSingleton(new Administrator());
    services.AddSingleton(new CallCenter());
    services.AddSingleton(new SoftwareDeveloper());

    return services;
}
