﻿using ChainOfResponsibility.Class;
using ChainOfResponsibility.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChainOfResponsibility
{
    internal class Application
    {
        private CallCenter _callCenter;
        private Administrator _administrator;
        private SoftwareDeveloper _softwareDeveloper;
        public Application(CallCenter callCenter, Administrator administrator, SoftwareDeveloper softwareDeveloper)
        {
            _callCenter = callCenter;
            _administrator = administrator;
            _softwareDeveloper = softwareDeveloper;
            callCenter.SetSupervisor(administrator);
            administrator.SetSupervisor(softwareDeveloper);
        }
        public void Run()
        {
            Console.WriteLine("Witaj");
            while(true)
            {
                Console.WriteLine("Wybierz problem w którym mamy Ci pomóc:");
                Console.WriteLine("1: Reset hasła");
                Console.WriteLine("2: Nie otrzymałem maila");
                Console.WriteLine("3: Stwórz użytkownika");
                Console.WriteLine("4: Przyznaj uprawnienie");
                Console.WriteLine("5: Serwer nie działa");

                string userInput = Console.ReadLine() ?? "";
                if (!ValidateUserInput(userInput, new[] { "1", "2", "3", "4", "5" }))
                    continue;

                Console.Clear();
                EProblem problem = (EProblem)Enum.Parse(typeof(EProblem), userInput);
                _callCenter.CheckProblem(problem);
                Console.ReadLine();
                Console.Clear();
            }
        }

        private bool ValidateUserInput(string userInput, string[] validValues)
        {
            if (String.IsNullOrEmpty(userInput) || !validValues.Contains(userInput))
            {
                Console.Clear();
                Console.WriteLine(@"
                    Wprowadzono niepoprawną wartość, spróbuj jeszcze raz.
                    ");
                return false;
            }
            return true;
        }
    }
}
