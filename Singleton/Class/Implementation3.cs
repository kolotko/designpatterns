﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singleton.Class
{
    //Lazy Initialization
    public class Implementation3
    {
        private static Lazy<Implementation3> instance = new Lazy<Implementation3>(() => new Implementation3());
        private Implementation3() 
        {
            Console.WriteLine($@"Poniższy komunikat powinien wystąpić tylko raz");
            Console.WriteLine($@"Tworzenie trzeciej implementacji singleton");
        }
        public static Implementation3 Instance { get { return instance.Value; } }
    }
}
