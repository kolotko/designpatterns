﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singleton.Class
{
    //Lazy Initialization
    //lock should be used to protect against multithreading and generating multiple singletons
    public class Implementation1
    {
        private static Implementation1 instance;
        private static readonly object instanceLock = new object();
        private Implementation1() { }

        public static Implementation1 Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (instanceLock)
                    {
                        if (instance == null)
                        {
                            Console.WriteLine($@"Poniższy komunikat powinien wystąpić tylko raz");
                            Console.WriteLine($@"Tworzenie pierwszej implementacji singleton");
                            instance = new Implementation1();
                        }
                    }
                }

                return instance;
            }
        }
    }
}
