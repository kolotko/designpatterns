﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Singleton.Class
{
    //Eager Initialization
    public class Implementation2
    {
        private static Implementation2 instance = new Implementation2();
        private Implementation2() { }
        //a static constructor can only be called once in the lifetime of a class. Is a protection against multithreading
        static Implementation2() 
        {
            Console.WriteLine($@"Poniższy komunikat powinien wystąpić tylko raz");
            Console.WriteLine($@"Tworzenie drugiej implementacji singleton");
        }
        public static Implementation2 Instance { get { return instance; } }
    }
}
