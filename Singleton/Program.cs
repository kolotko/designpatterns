﻿using Singleton.Class;




Console.WriteLine($@"Pierwsza implementacja:");
Parallel.Invoke(
                () => GetImplementation1(),
                () => GetImplementation1(),
                () => GetImplementation1(),
                () => GetImplementation1(),
                () => GetImplementation1(),
                () => GetImplementation1(),
                () => GetImplementation1(),
                () => GetImplementation1(),
                () => GetImplementation1(),
                () => GetImplementation1()
                );

Console.WriteLine("");
Console.WriteLine("");
Console.WriteLine($@"Druga implementacja:");
Parallel.Invoke(
                () => GetImplementation2(),
                () => GetImplementation2(),
                () => GetImplementation2(),
                () => GetImplementation2(),
                () => GetImplementation2(),
                () => GetImplementation2(),
                () => GetImplementation2(),
                () => GetImplementation2(),
                () => GetImplementation2(),
                () => GetImplementation2()
                );

Console.WriteLine("");
Console.WriteLine("");
Console.WriteLine($@"Trzecia implementacja:");
Parallel.Invoke(
                () => GetImplementation3(),
                () => GetImplementation3(),
                () => GetImplementation3(),
                () => GetImplementation3(),
                () => GetImplementation3(),
                () => GetImplementation3(),
                () => GetImplementation3(),
                () => GetImplementation3(),
                () => GetImplementation3(),
                () => GetImplementation3(),
                () => GetImplementation3()
                );
Console.ReadLine();



void GetImplementation1()
{
    var implementation1 = Implementation1.Instance;
}

void GetImplementation2()
{
    var implementation2 = Implementation2.Instance;
}

void GetImplementation3()
{
    var implementation3 = Implementation3.Instance;
}