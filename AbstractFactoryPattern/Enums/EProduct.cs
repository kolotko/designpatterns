﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactoryPattern.Enums
{
    public enum EProduct
    {
        Shirt = 1,
        Shorts = 2
    }
}
