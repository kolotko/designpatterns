﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactoryPattern.Enums
{
    public enum EClothesFactory
    {
        Sport = 1,
        Summer = 2,
    }
}
