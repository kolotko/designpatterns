﻿using AbstractFactoryPattern.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactoryPattern.Products
{
    public class SummerShirt : IShirt
    {
        public string GetShirtDescription()
        {
            return "letnia koszulka";
        }
    }
}
