﻿using AbstractFactoryPattern.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactoryPattern.Products
{
    public class SummerShorts : IShorts
    {
        public string GetShortsDescription()
        {
            return "letnie spodenki";
        }
    }
}
