﻿using AbstractFactoryPattern.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactoryPattern.Products
{
    public class SportsShirt : IShirt
    {
        public string GetShirtDescription()
        {
            return "sportowa koszulka";
        }
    }
}
