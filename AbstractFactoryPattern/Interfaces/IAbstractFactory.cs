﻿using AbstractFactoryPattern.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactoryPattern.Interfaces
{
    public interface IAbstractFactory
    {
        public IClothesFactory GetClothesFactory(EClothesFactory eClothesFactory);
    }
}
