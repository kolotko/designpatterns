﻿using AbstractFactoryPattern.Enums;
using AbstractFactoryPattern.Factory;
using AbstractFactoryPattern.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactoryPattern
{
    public class Application
    {
        private ClothesFactory _clothesFactory;
        private EClothesFactory _eElothesFactory;
        private EProduct _eProduct;
        private IClothesFactory _selectedFactory;
        public Application(ClothesFactory clothesFactory)
        {
            _clothesFactory = clothesFactory;
        }

        public void Run()
        {
            Console.WriteLine("Witaj w naszym sklepie z odzieżą.");
            Console.WriteLine("");
            Console.WriteLine("");

            while (true)
            {
                SelectType();
                Console.Clear();
                SelectClothes();
                Console.Clear();
            }
        }

        private void SelectType()
        {
            while (true)
            {
                Console.WriteLine("Jaka odzież Cię interesuje?");
                Console.WriteLine("1: Sportowa");
                Console.WriteLine("2: Letnia");

                string userInput = Console.ReadLine() ?? "";
                if (String.IsNullOrEmpty(userInput) || !new[] { "1", "2" }.Contains(userInput))
                {
                    Console.Clear();
                    Console.WriteLine("Wprowadzono niepoprawną wartość, spróbuj jeszcze raz.");
                    Console.WriteLine("");
                    Console.WriteLine("");
                    continue;
                }
                _eElothesFactory = (EClothesFactory)Enum.Parse(typeof(EClothesFactory), userInput);
                _selectedFactory = _clothesFactory.GetClothesFactory(_eElothesFactory);

                return;
            }
        }

        private void SelectClothes()
        {
            while (true)
            {
                Console.WriteLine("Wybierz część garderoby");
                Console.WriteLine("1: Koszulka");
                Console.WriteLine("2: Spodnie");

                string userInput = Console.ReadLine() ?? "";
                if (String.IsNullOrEmpty(userInput) || !new[] { "1", "2" }.Contains(userInput))
                {
                    Console.Clear();
                    Console.WriteLine("Wprowadzono niepoprawną wartość, spróbuj jeszcze raz.");
                    Console.WriteLine("");
                    Console.WriteLine("");
                    continue;
                }

                _eProduct = (EProduct)Enum.Parse(typeof(EProduct), userInput);
                Console.Clear();
                switch (_eProduct)
                {
                    case EProduct.Shirt:
                        Console.WriteLine(_selectedFactory.GetShirt().GetShirtDescription());
                        break;
                    case EProduct.Shorts:
                        Console.WriteLine(_selectedFactory.GetShorts().GetShortsDescription());
                        break;
                }
                Console.WriteLine("");
                Console.WriteLine("");
                Console.WriteLine("Wprowadź dowolną wartośc by wyjść");
                userInput = Console.ReadLine() ?? "";
                return;
            }
        }
    }
}
