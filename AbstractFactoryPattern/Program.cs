﻿using AbstractFactoryPattern;
using AbstractFactoryPattern.Factory;
using Microsoft.Extensions.DependencyInjection;

var services = ConfigureServices();
var servicesProvider = services.BuildServiceProvider();
servicesProvider.GetService<Application>()?.Run();

/// <summary>
/// Register services
/// </summary>
static IServiceCollection ConfigureServices()
{
    IServiceCollection services = new ServiceCollection();

    services.AddSingleton(new ClothesFactory());

    services.AddSingleton<Application>();

    return services;
}