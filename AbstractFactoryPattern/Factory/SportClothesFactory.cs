﻿using AbstractFactoryPattern.Interfaces;
using AbstractFactoryPattern.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactoryPattern.Factory
{
    public class SportClothesFactory : IClothesFactory
    {
        public IShirt GetShirt()
        {
            return new SportsShirt();
        }

        public IShorts GetShorts()
        {
            return new SportsShorts();
        }
    }
}
