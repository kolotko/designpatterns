﻿using AbstractFactoryPattern.Enums;
using AbstractFactoryPattern.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactoryPattern.Factory
{
    public class ClothesFactory : IAbstractFactory
    {
        public IClothesFactory GetClothesFactory(EClothesFactory eClothesFactory)
        {
            switch (eClothesFactory)
            {
                case EClothesFactory.Sport:
                    return new SportClothesFactory();
                case EClothesFactory.Summer:
                    return new SummerClothesFactory();
            }

            throw new Exception("Niestety nie mamy takiej kolekcji ubrań");
        }
    }
}
