﻿using AbstractFactoryPattern.Interfaces;
using AbstractFactoryPattern.Products;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractFactoryPattern.Factory
{
    public class SummerClothesFactory : IClothesFactory
    {
        public IShirt GetShirt()
        {
            return new SummerShirt();
        }

        public IShorts GetShorts()
        {
            return new SummerShorts();
        }
    }
}
