﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TemplateMethod.Abstraction;

namespace TemplateMethod.Class
{
    internal class Pepperoni : PizzaTemplateMethod
    {
        public override string name => "Pepperoni";

        protected override void PizzaDough()
        {
            Console.WriteLine("- na grubym cieście");
        }
        protected override void Sauce()
        {
            Console.WriteLine("- sos pomidorowy");
        }

        protected override void Addition1()
        {
            Console.WriteLine("- ostre peperoni");
        }
        protected override void Addition2()
        {
            Console.WriteLine("- peperoni azjatyckie");
        }
    }
}
