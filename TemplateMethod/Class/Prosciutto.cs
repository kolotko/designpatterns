﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TemplateMethod.Abstraction;

namespace TemplateMethod.Class
{
    internal class Prosciutto : PizzaTemplateMethod
    {
        public override string name => "Prosciutto";

        protected override void PizzaDough()
        {
            Console.WriteLine("- na cienkim cieście");
        }
        protected override void Sauce()
        {
            Console.WriteLine("- sos śmietanowy");
        }

        protected override void Addition1()
        {
            Console.WriteLine("- szynka");
        }
        protected override void Addition2()
        {
            Console.WriteLine("- ser");
        }
    }
}
