﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TemplateMethod.Abstraction;

namespace TemplateMethod.Class
{
    internal class Cheese : PizzaTemplateMethod
    {
        public override string name => "Cheese";

        protected override void PizzaDough()
        {
            Console.WriteLine("- na cienkim cieście");
        }
        protected override void Sauce()
        {
            Console.WriteLine("- sos pomidorowy");
        }

        protected override void Addition1()
        {
            Console.WriteLine("- ser chedar");
        }
        protected override void Addition2()
        {
            Console.WriteLine("- gouda");
        }
    }
}
