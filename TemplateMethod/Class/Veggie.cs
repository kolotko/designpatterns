﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TemplateMethod.Abstraction;

namespace TemplateMethod.Class
{
    internal class Veggie : PizzaTemplateMethod
    {
        public override string name { get => "Veggie"; }

        protected override void PizzaDough()
        {
            Console.WriteLine("- na grubym cieście");
        }
        protected override void Sauce()
        {
            Console.WriteLine("- sos pomidorowy");
        }

        protected override void Addition1()
        {
            Console.WriteLine("- papryka");
        }
        protected override void Addition2()
        {
            Console.WriteLine("- tofu");
        }

        protected override void Hook1() 
        {
            Console.WriteLine("* danie vegańskie");
        }
    }
}
