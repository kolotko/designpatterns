﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TemplateMethod.Class;
using TemplateMethod.Enums;

namespace TemplateMethod
{
    internal class Application
    {
        private readonly Cheese _cheese;
        private readonly Pepperoni _pepperoni;
        private readonly Prosciutto _prosciutto;
        private readonly Veggie _veggie;

        public Application(Cheese cheese, Pepperoni pepperoni, Prosciutto prosciutto, Veggie veggie)
        {
            _cheese = cheese;
            _pepperoni = pepperoni;
            _prosciutto = prosciutto;
            _veggie = veggie;
        }
        public void Run()
        {
            Console.WriteLine("Witaj w naszej pizzeri.");
            Console.WriteLine("Zachędamy do przejrzenia naszego menu:");
            while (true)
            {
                Console.WriteLine("Menu");
                Console.WriteLine($"1: {_prosciutto.name}");
                Console.WriteLine($"2: {_cheese.name}");
                Console.WriteLine($"3: {_veggie.name}");
                Console.WriteLine($"4: {_pepperoni.name}");

                string userInput = Console.ReadLine() ?? "";
                if (!ValidateUserInput(userInput, new[] { "1", "2", "3", "4" }))
                    continue;

                Console.Clear();
                EPizza pizza = (EPizza)Enum.Parse(typeof(EPizza), userInput);

                switch (pizza)
                {
                    case EPizza.Prosciutto:
                        _prosciutto.MakePizza();
                        break;
                    case EPizza.Cheese:
                        _cheese.MakePizza();
                        break;
                    case EPizza.Veggie:
                        _veggie.MakePizza();
                        break;
                    case EPizza.Pepperoni:
                        _pepperoni.MakePizza();
                        break;
                }

                Console.ReadLine();
                Console.Clear();
            }
        }

        private bool ValidateUserInput(string userInput, string[] validValues)
        {
            if (String.IsNullOrEmpty(userInput) || !validValues.Contains(userInput))
            {
                Console.Clear();
                Console.WriteLine(@"
                    Wprowadzono niepoprawną wartość, spróbuj jeszcze raz.
                    ");
                return false;
            }
            return true;
        }
    }
}
