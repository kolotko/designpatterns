﻿using Microsoft.Extensions.DependencyInjection;
using TemplateMethod;
using TemplateMethod.Class;

var services = ConfigureServices();
var servicesProvider = services.BuildServiceProvider();
servicesProvider.GetService<Application>()?.Run();

static IServiceCollection ConfigureServices()
{
    IServiceCollection services = new ServiceCollection();
    services.AddSingleton<Cheese>();
    services.AddSingleton<Pepperoni>();
    services.AddSingleton<Prosciutto>();
    services.AddSingleton<Veggie>();
    services.AddSingleton<Application>();

    return services;
}