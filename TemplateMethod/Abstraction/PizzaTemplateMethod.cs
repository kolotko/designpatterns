﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TemplateMethod.Abstraction
{
    internal abstract class PizzaTemplateMethod
    {
        public abstract string name { get; }
        public void MakePizza()
        {
            PizzaDough();
            Sauce();
            Addition1();
            Addition2();
            Bake();
            Hook1();
        }

        protected abstract void PizzaDough();
        protected abstract void Sauce();

        protected abstract void Addition1();
        protected abstract void Addition2();
        protected void Bake()
        {
            Console.WriteLine("Czas pieczenia: 30 min.");
        }
        protected virtual void Hook1() { }
    }
}
