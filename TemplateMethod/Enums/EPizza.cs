﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TemplateMethod.Enums
{
    internal enum EPizza
    {
        Prosciutto = 1,
        Cheese = 2,
        Veggie = 3,
        Pepperoni = 4
    }
}
