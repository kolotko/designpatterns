﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryMethod.Enums
{
    public enum WindowType
    {
        Fixed = 1,
        Slide = 2,
        Double = 3
    }
}
