﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryMethod.Enums
{
    public enum WindowMode
    {
        Open = 1,
        Close = 2
    }
}
