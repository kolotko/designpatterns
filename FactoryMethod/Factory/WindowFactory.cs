﻿using FactoryMethod.Class;
using FactoryMethod.Enums;
using FactoryMethod.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryMethod.Factory
{
    public class WindowFactory
    {
        public IWindow GetWindow(WindowType windowType)
        {
            switch (windowType)
            {
                case WindowType.Fixed:
                    return new FixedWindow();
                case WindowType.Slide:
                    return new SlideWindow();
                case WindowType.Double:
                    return new DoubleWindow();
            }
            throw new Exception("Przykro nam ale nie mamy na magazynie okna tego typu");
        }
    }
}
