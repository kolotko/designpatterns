﻿using FactoryMethod.Enums;
using FactoryMethod.Factory;
using FactoryMethod.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryMethod
{
    public class Application
    {
        private WindowFactory _windowFactory;
        private WindowMode _windowMode;
        private IWindow _selectedWindow;
        public Application(WindowFactory windowFactory)
        {
            _windowFactory = windowFactory;
        }

        public void Run()
        {
            Console.WriteLine("Witaj w naszym salonie okien.");
            Console.WriteLine("");
            Console.WriteLine("");
            
            while (true)
            {
                SelectType();
                Console.Clear();
                SelectMode();
                Console.Clear();
            }
        }

        private void SelectType()
        {
            while (true)
            {
                Console.WriteLine("Wybierz okno");
                Console.WriteLine("1: Okno z brakiem możliwości otwarcia");
                Console.WriteLine("2: Okno przesuwane");
                Console.WriteLine("3: Okno podwójne");

                string userInput = Console.ReadLine() ?? "";
                if (String.IsNullOrEmpty(userInput) || !new[] { "1", "2", "3" }.Contains(userInput))
                {
                    Console.Clear();
                    Console.WriteLine("Wprowadzono niepoprawną wartość, spróbuj jeszcze raz.");
                    Console.WriteLine("");
                    Console.WriteLine("");
                    continue;
                }
                WindowType windowType = (WindowType)Enum.Parse(typeof(WindowType), userInput);
                _selectedWindow = _windowFactory.GetWindow(windowType);

                return;
            }
        }

        private void SelectMode()
        {
            while (true)
            {
                Console.WriteLine("Wybierz co chesz zrobić z oknem");
                Console.WriteLine("1: Otworzyć");
                Console.WriteLine("2: Zamknąć");

                string userInput = Console.ReadLine() ?? "";
                if (String.IsNullOrEmpty(userInput) || !new[] { "1", "2" }.Contains(userInput))
                {
                    Console.Clear();
                    Console.WriteLine("Wprowadzono niepoprawną wartość, spróbuj jeszcze raz.");
                    Console.WriteLine("");
                    Console.WriteLine("");
                    continue;
                }

                _windowMode = (WindowMode)Enum.Parse(typeof(WindowMode), userInput);
                Console.Clear();
                switch (_windowMode)
                {
                    case WindowMode.Open:
                        Console.WriteLine(_selectedWindow.Open());
                        break;
                    case WindowMode.Close:
                        Console.WriteLine(_selectedWindow.Close());
                        break;
                }
                Console.WriteLine("");
                Console.WriteLine("");
                Console.WriteLine("Wprowadź dowolną wartośc by wyjść");
                userInput = Console.ReadLine() ?? "";
                return;
            }    
        }
    }
}
