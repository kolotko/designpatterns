﻿using FactoryMethod.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryMethod.Class
{
    public class SlideWindow : IWindow
    {
        public string Close()
        {
            return "Przesuwane okno zostało zamknięte";
        }

        public string Open()
        {
            return "Przesuwane okno zostało otwarte";
        }
    }
}
