﻿using FactoryMethod.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryMethod.Class
{
    public class FixedWindow : IWindow
    {
        public string Close()
        {
            return "To okno nie może zostać zamknięte";
        }

        public string Open()
        {
            return "To okno nie może zostać otwarte";
        }
    }
}
