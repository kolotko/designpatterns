﻿using FactoryMethod.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryMethod.Class
{
    public class DoubleWindow : IWindow
    {
        public string Close()
        {
            return "Podwójne okno zostało zamknięte";
        }

        public string Open()
        {
            return "Podwójne okno zostało otwarte";
        }
    }
}
