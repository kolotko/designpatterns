﻿using Observer.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Observer
{
    internal class Application
    {
        private Store _store;
        private Customer _customer;
        public Application(Store store, Customer customer)
        {
            _store = store;
            _customer = customer;
        }

        public void Run()
        {
            while (true)
            {
                Console.WriteLine("Dziękujemy za zamówienie produktu w naszym sklepie. Otrzymasz powiadomieniao statusie twojego produktu");
                _customer.Subscribe(_store);

                Task.Delay(1000).Wait();
                _store.SendEvent("Wysłano zamówiony produkt");
                Task.Delay(1000).Wait();
                _store.DeliveredEvent();

                _customer.Unsubscribe();
                Console.ReadLine();
            }
        }
    }
}
