﻿using Microsoft.Extensions.DependencyInjection;
using Observer;
using Observer.Class;

var services = ConfigureServices();
var servicesProvider = services.BuildServiceProvider();
servicesProvider.GetService<Application>()?.Run();

/// <summary>
/// Register services
/// </summary>
static IServiceCollection ConfigureServices()
{
    IServiceCollection services = new ServiceCollection();

    services.AddSingleton<Application>();
    services.AddSingleton(new Store());
    services.AddSingleton(new Customer());

    return services;
}