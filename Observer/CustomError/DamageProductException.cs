﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Observer.CustomError
{
    internal class DamageProductException : Exception
    {
        public DamageProductException(string message) : base(message)
        { }
    }
}
