﻿using Observer.CustomError;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Observer.Class
{
    internal class Store : IObservable<Supply>
    {
        private List<IObserver<Supply>> _observers;
        
        public Store()
        {
            _observers = new();
        }

        public IDisposable Subscribe(IObserver<Supply> observer)
        {
            if (!_observers.Contains(observer))
                _observers.Add(observer);
            return new Unsubscriber(_observers, observer);
        }

        public void SendEvent(string description)
        {
            try
            {
                foreach (var observer in _observers)
                {
                    observer.OnNext(new Supply(description,
                                    DateTime.Now));
                }
            }
            catch (Exception)
            {

                ErrorEvent();
            }
            
        }

        public void DeliveredEvent()
        {
            try
            {
                foreach (var observer in _observers)
                {
                    observer.OnCompleted();
                }
            }
            catch
            {

                ErrorEvent();
            }
            
        }

        private void ErrorEvent()
        {
            foreach (var observer in _observers)
            {
                observer.OnError(new DamageProductException("Twój produkt uległ uszkodzeniu"));
            }
        }

        // Define Unsubscriber class
        private class Unsubscriber : IDisposable
        {
            private List<IObserver<Supply>> _observers;
            private IObserver<Supply> _observer;
            public Unsubscriber(List<IObserver<Supply>> observers, IObserver<Supply> observer)
            {
                this._observers = observers;
                this._observer = observer;
            }
            public void Dispose()
            {
                if (!(_observer == null)) _observers.Remove(_observer);
            }
        }
    }
}
