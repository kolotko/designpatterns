﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Observer.Class
{
    internal class Customer : IObserver<Supply>
    {
        private IDisposable _unsubscriber;

        public virtual void Subscribe(IObservable<Supply> provider)
        {
            if (provider != null)
                _unsubscriber = provider.Subscribe(this);
        }
        public void OnCompleted()
        {
            Console.WriteLine("Dostarczono");
        }

        public void OnError(Exception error)
        {
            Console.WriteLine($"Uszkodzenie: {error.Message}");
        }

        public void OnNext(Supply value)
        {
            Console.WriteLine($"{value.Date}: {value.Description}");
        }

        public virtual void Unsubscribe()
        {
            _unsubscriber.Dispose();
        }
    }
}
