﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Observer.Class
{
    internal class Supply
    {
        public string Description { get; set; }
        public DateTime Date { get; set; }
        public Supply(string description, DateTime date)
        {
            Description = description;
            Date = date;
        }
    }
}
