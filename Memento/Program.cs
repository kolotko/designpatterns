﻿using Memento;
using Memento.Class;
using Memento.Interfaces;
using Microsoft.Extensions.DependencyInjection;

var services = ConfigureServices();
var servicesProvider = services.BuildServiceProvider();
servicesProvider.GetService<Application>()?.Run();

/// <summary>
/// Register services
/// </summary>
static IServiceCollection ConfigureServices()
{
    IServiceCollection services = new ServiceCollection();

    services.AddSingleton<Application>();
    services.AddSingleton<IMementoManager>(new MementoManager());
    services.AddSingleton<IChampion>(new Champion());

    return services;
}