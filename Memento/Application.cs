﻿using Memento.Enums;
using Memento.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Memento
{
    internal class Application
    {
        private readonly IMementoManager _mementoManager;
        private readonly IChampion _champion;
        public Application(IMementoManager mementoManager, IChampion champion)
        {
            _mementoManager = mementoManager;
            _champion = champion;
        }
        public void Run()
        {
            Console.WriteLine("Witaj w naszej grze.");
            Console.WriteLine("Wprowadź imię dla swojej postaci:");
            _champion.Name = Console.ReadLine() ?? "";
            Console.Clear();
            while (true)
            {
                Console.WriteLine("Wybierz co chcesz zrobić:");
                Console.WriteLine("1: dodaj level");
                Console.WriteLine("2: zapisz");
                Console.WriteLine("3: wczytaj ostatni zapis");
                Console.WriteLine("Uwaga wczytując stan bohatera (3) zapis zostaje usunięty");


                string userInput = Console.ReadLine() ?? "";
                if (!ValidateUserInput(userInput, new[] { "1", "2", "3" }))
                    continue;

                Console.Clear();
                EChampionAction championAction = (EChampionAction)Enum.Parse(typeof(EChampionAction), userInput);
                switch (championAction)
                {
                    case EChampionAction.AddLevel:
                        _champion.AddLevel();
                        break;
                    case EChampionAction.Save:
                        var save = _champion.Save();
                        _mementoManager.AddMemento(save);
                        break;
                    case EChampionAction.LoadLastSave:
                        var load = _mementoManager.GetMemento();
                        if (load != null)
                            _champion.Load(load);
                        break;
                }
            }
        }

        private bool ValidateUserInput(string userInput, string[] validValues)
        {
            if (String.IsNullOrEmpty(userInput) || !validValues.Contains(userInput))
            {
                Console.Clear();
                Console.WriteLine(@"
                    Wprowadzono niepoprawną wartość, spróbuj jeszcze raz.
                    ");
                return false;
            }
            return true;
        }
    }
}
