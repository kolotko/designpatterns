﻿using Memento.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Memento.Class
{
    internal class MementoManager : IMementoManager
    {
        private readonly Stack<MementoChampion> _mementos = new Stack<MementoChampion>();
        public void AddMemento(MementoChampion memento) => _mementos.Push(memento);
        public MementoChampion GetMemento()
        {
            if (_mementos.Count == 0)
            {
                Console.WriteLine("Niestety nie masz jeszcze żadnego zapiu");
                return null;
            }
            return _mementos.Pop();
        }
    }
}
