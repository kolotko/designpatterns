﻿using Memento.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Memento.Class
{
    internal record MementoChampion(int Level, string Name);
    internal class Champion : IChampion
    {
        public int Level { get; set; }
        public string Name { get; set; }

        public Champion()
        {
            Name = "Bez imienia";
            Level = 1;
        }

        public MementoChampion Save()
        {
            Console.WriteLine($"Zapisano stan bohatera: {Name} o poziomie {Level}");
            return new MementoChampion(Level, Name);
        }
        public void Load(MementoChampion memento)
        {
            Level = memento.Level;
            Name = memento.Name;
            Console.WriteLine($"Wczytano stan bohatera: {Name} o poziomie {Level}");
        }

        public void AddLevel()
        {
            Level = Level + 1;
            Console.WriteLine($"{Name} awansował na poziom: {Level}");
        }
    }
}
