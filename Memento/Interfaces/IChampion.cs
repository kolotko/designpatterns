﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Memento.Class;

namespace Memento.Interfaces
{
    internal interface IChampion
    {
        int Level { get; set; }
        string Name { get; set; }
        MementoChampion Save();
        void Load(MementoChampion memento);
        void AddLevel();
    }
}
