﻿using Memento.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Memento.Interfaces
{
    internal interface IMementoManager
    {
        void AddMemento(MementoChampion memento);
        MementoChampion GetMemento();
    }
}
