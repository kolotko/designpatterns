﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Memento.Enums
{
    internal enum EChampionAction
    {
        AddLevel = 1,
        Save = 2,
        LoadLastSave = 3
    }
}
