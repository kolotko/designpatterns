﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decorator.Abstraction
{
    public abstract class Car
    {
        public abstract string CreateCar();

        public bool Validate(string name)
        {
            if (!CreateCar().Contains(name))
                return true;

            return false;
        }
    }
}
