﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decorator.Abstraction
{
    public abstract class AdditionalFunctionality : Car
    {
        protected Car _car;
        public string name;

        public AdditionalFunctionality(Car car)
        {
            _car = car;
        }

        public void SetFunctionality(Car car)
        {
            _car = car;
        }

        public override string CreateCar()
        {
            if (_car != null)
                return _car.CreateCar();

            return string.Empty;
        }
    }
}
