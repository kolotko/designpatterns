﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decorator.Enums
{
    public enum ECar
    {
        Skoda = 1,
        Fiat = 2,
    }
}
