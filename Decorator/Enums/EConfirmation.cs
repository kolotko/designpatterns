﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decorator.Enums
{
    public enum EConfirmation
    {
        Yes = 1,
        No = 2
    }
}
