﻿using Decorator.Abstraction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decorator.Class
{
    public class Fiat : Car
    {
        public override string CreateCar()
        {
            return @"
                      - podstawowe wyposarzenie fiata";
        }
    }
}
