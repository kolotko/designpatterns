﻿using Decorator.Abstraction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decorator.Class
{
    public class ElectricWindows : AdditionalFunctionality
    {
        public ElectricWindows(Car car) : base(car)
        {
            name = "elektryczne szyby";
        }

        public override string CreateCar()
        {
            return @$"
                      - {name}
                      {base.CreateCar()}";
        }
    }
}
