﻿using Decorator.Abstraction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decorator.Class
{
    internal class AirConditioning : AdditionalFunctionality
    {
        public AirConditioning(Car car) : base(car)
        {
            name = "klimatyzacja";
        }

        public override string CreateCar()
        {
            return @$"
                      - {name}
                      {base.CreateCar()}";
        }
    }
}
