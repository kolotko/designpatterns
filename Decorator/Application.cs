﻿using Decorator.Abstraction;
using Decorator.Class;
using Decorator.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Decorator
{
    public class Application
    {
        private Car _car = null;
        public void Run()
        {
            while (true)
            {
                CreateCar();
                Console.Clear();
                SelectFunctionality();
                Console.Clear();
                DisplayCar();
            }
        }

        public void CreateCar()
        {
            Console.Clear();
            while (true)
            {
                Console.WriteLine(@"
                                Witaj w naszym salonie samochodowym. 
                                Wybierz auto:
                                1. Skoda
                                2. Fiat");

                string userInput = Console.ReadLine() ?? "";
                if (!ValidateUserInput(userInput, new[] { "1", "2" }))
                    continue;

                ECar car = (ECar)Enum.Parse(typeof(ECar), userInput);

                switch (car)
                {
                    case ECar.Fiat:
                        _car = new Fiat();
                        return;
                    case ECar.Skoda:
                        _car = new Skoda();
                        return;
                }
            }
                
        }

        public void SelectFunctionality()
        {
            Console.Clear();
            while (true)
            {
                Console.WriteLine(@"
                                Wybierz wyposarzenie dla swojego auta:
                                1. Klimatyzacja
                                2. Elektryczne szyby");

                string userInput = Console.ReadLine() ?? "";
                if (!ValidateUserInput(userInput, new[] { "1", "2" }))
                    continue;

                EFunctionality functionality = (EFunctionality)Enum.Parse(typeof(EFunctionality), userInput);

                switch (functionality)
                {
                    case EFunctionality.AirConditioning:
                       var  airConditioning = new AirConditioning(_car);
                        if(_car.Validate(airConditioning.name))
                            _car = airConditioning;
                        break;
                    case EFunctionality.ElectricWindows:
                        var electricWindows = new ElectricWindows(_car);
                        if(_car.Validate(electricWindows.name))
                            _car = electricWindows;
                        break;
                }

                Console.Clear();
                Console.WriteLine(@"
                                Czy to wszystkie dodatki do samochodu jakie chcesz wybrać?:
                                1. Tak
                                2. Nie");

                userInput = Console.ReadLine() ?? "";
                if (!ValidateUserInput(userInput, new[] { "1", "2" }))
                    continue;

                EConfirmation confirmation = (EConfirmation)Enum.Parse(typeof(EConfirmation), userInput);

                switch (confirmation)
                {
                    case EConfirmation.Yes:
                        return;
                    case EConfirmation.No:
                        Console.Clear();
                        continue;
                }
            }
        }

        public void DisplayCar()
        {
            Console.WriteLine(@"
                      Twoje auto posiada:" + 
                                 _car.CreateCar());
            string userInput = Console.ReadLine() ?? "";
            return;
        }

        private bool ValidateUserInput(string userInput, string[] validValues)
        {
            if (String.IsNullOrEmpty(userInput) || !validValues.Contains(userInput))
            {
                Console.Clear();
                Console.WriteLine(@"
                    Wprowadzono niepoprawną wartość, spróbuj jeszcze raz.
                    ");
                return false;
            }
            return true;
        }
    }
}
