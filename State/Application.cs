﻿using State.Class;
using State.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace State
{
    internal class Application
    {
        private readonly H2O _h2o;
        private readonly Ice _ice;
        private readonly Steam _steam;
        private readonly Liquid _liquid;

        public Application(H2O h2o, Ice ice, Steam steam, Liquid liquid)
        {
            _h2o = h2o;
            _ice = ice;
            _steam = steam;
            _liquid = liquid;
        }
        public void Run()
        {
            Console.WriteLine("Witaj w naszym labolatorium.");
            Console.WriteLine("Potrafimy zmienić h2o w dowolny stan.");
            while (true)
            {
                Console.WriteLine("Wybierz stan:");
                Console.WriteLine("1: woda");
                Console.WriteLine("2: lód");
                Console.WriteLine("3: para wodna");

                string userInput = Console.ReadLine() ?? "";
                if (!ValidateUserInput(userInput, new[] { "1", "2", "3" }))
                    continue;

                Console.Clear();
                EState state = (EState)Enum.Parse(typeof(EState), userInput);

                switch (state)
                {
                    case EState.Liquid:
                        _h2o.TransitionTo(_liquid);
                        _h2o.Get();
                        break;
                    case EState.Ice:
                        _h2o.TransitionTo(_ice);
                        _h2o.Get();
                        break;
                    case EState.Steam:
                        _h2o.TransitionTo(_steam);
                        _h2o.Get();
                        break;
                }

                Console.ReadLine();
                Console.Clear();
            }
        }

        private bool ValidateUserInput(string userInput, string[] validValues)
        {
            if (String.IsNullOrEmpty(userInput) || !validValues.Contains(userInput))
            {
                Console.Clear();
                Console.WriteLine(@"
                    Wprowadzono niepoprawną wartość, spróbuj jeszcze raz.
                    ");
                return false;
            }
            return true;
        }
    }
}
