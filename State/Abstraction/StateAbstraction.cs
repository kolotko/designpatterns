﻿using State.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace State.Abstraction
{
    internal abstract class StateAbstraction
    {
        protected H2O _context;

        public void SetContext(H2O context)
        {
            this._context = context;
        }

        public abstract void Get();
    }
}
