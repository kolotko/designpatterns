﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace State.Enums
{
    internal enum EState
    {
        Liquid = 1,
        Ice = 2,
        Steam = 3
    }
}
