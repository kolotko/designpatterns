﻿using Microsoft.Extensions.DependencyInjection;
using State;
using State.Class;

var services = ConfigureServices();
var servicesProvider = services.BuildServiceProvider();
servicesProvider.GetService<Application>()?.Run();

static IServiceCollection ConfigureServices()
{
    IServiceCollection services = new ServiceCollection();
    services.AddSingleton<H2O>();
    services.AddSingleton<Ice>();
    services.AddSingleton<Steam>();
    services.AddSingleton<Liquid>();
    services.AddSingleton<Application>();

    return services;
}