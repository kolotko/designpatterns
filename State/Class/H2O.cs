﻿using State.Abstraction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace State.Class
{
    internal class H2O
    {
        private StateAbstraction _state;
        public H2O() { }

        public H2O(StateAbstraction state)
        {
            TransitionTo(state);
        }

        public void TransitionTo(StateAbstraction state)
        {
            _state = state;
            _state.SetContext(this);
        }

        public void Get()
        {
            _state.Get();
        }
    }
}
