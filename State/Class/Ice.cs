﻿using State.Abstraction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace State.Class
{
    internal class Ice : StateAbstraction
    {
        public override void Get()
        {
            Console.WriteLine("Zmiana w lód");
        }
    }
}
