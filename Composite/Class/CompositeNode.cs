﻿using Composite.Abstraction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Composite.Class
{
    public class CompositeNode : Node
    {
        private List<Node> childrenNode;
        private readonly string name;
        private readonly Node? parentNode;
        public override string Name => name;

        public override List<Node> Children => childrenNode;

        public CompositeNode(string name, Node? parentNode = null)
        {
            this.name = name;
            this.parentNode = parentNode;
            this.childrenNode = new ();

        }

        public override void Add(Node node)
        {
            childrenNode.Add(node);
        }
        public override void Remove(Node node)
        {
            childrenNode.Remove(node);
        }

        public override void PrintChildren()
        {
            Console.WriteLine($"Zawartość {name.ToLower()}: {string.Join(", ", childrenNode.Select(c => c.Name))}");
        }
    }
}
