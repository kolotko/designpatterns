﻿using Composite.Abstraction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Composite.Class
{
    public class CompositeLeaf : Node
    {
        private readonly string name;
        private readonly Node? parentNode;
        public override string Name => name;
        public CompositeLeaf(string name, Node parentNode)
        {
            this.name = name;
            this.parentNode = parentNode;
        }

        public override void PrintChildren() {}
    }
}
