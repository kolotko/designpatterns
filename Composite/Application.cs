﻿using Composite.Abstraction;
using Composite.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Composite
{
    public class Application
    {
        public void Run()
        {
            var root = new CompositeNode("Paczka główna");
            var leaf1 = new CompositeLeaf("Klocki lego", root);
            var node1 = new CompositeNode("Mniejsza paczka", root);
            var leaf2 = new CompositeLeaf("Złoty naszyjnik", node1);

            root.Add(leaf1);
            root.Add(node1);
            node1.Add(leaf2);

            Console.WriteLine("Przygotowaliśmy dla Ciebie paczkę. o następującej zawartości:");
            DisplayContents(root);
            Console.ReadLine();
        }

        private void DisplayContents(Node node)
        {
            node.PrintChildren();
            #pragma warning disable CS8602 // Wyłuskanie odwołania, które może mieć wartość null.
            foreach (Node child in node.Children)
            #pragma warning restore CS8602 // Wyłuskanie odwołania, które może mieć wartość null.
            {
                if (child.Children == null)
                    continue;
                DisplayContents(child);
            }
        }
    }
}
