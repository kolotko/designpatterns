﻿using Mediator.Class;
using Mediator.Interfaces;

namespace Mediator
{
    public class Application
    {
        private readonly IOrdersCenter _ordersCenter;
        public Application(IOrdersCenter ordersCenter)
        {
            _ordersCenter = ordersCenter;
        }

        public void Run()
        {
            CreateRestaurants();
            while (true)
            {
                Console.WriteLine(@"
                                    Witaj
                                    Podaj swoją lokalizację, a my stworzymy dla Ciebie zamówienie w restauracji.
                                    1:  Dolnośląskie
                                    2:  Kujawsko-pomorskie
                                    3:  Lubelskie
                                    4:  Lubuskie
                                    5:  Łódzkie
                                    6:  Małopolskie
                                    7:  Mazowieckie
                                    8:  Opolskie
                                    9:  Podkarpackie
                                    10: Podlaskiee
                                    11: Pomorskie
                                    12: Śląskie
                                    13: Świętokrzyskie
                                    14: Warmińsko-mazurskie
                                    15: Wielkopolskie
                                    16: Zachodniopomorskie
                ");

                var userInput = Console.ReadLine() ?? "";
                var location = 0;

                try
                {
                    location = Int32.Parse(userInput);
                    if (location > 16 || location < 1)
                        throw new Exception();
                }
                catch (Exception)
                {
                    Console.Clear();
                    Console.WriteLine("Wprowadzono niepoprawną wartość, spróbuj jeszcze raz");
                    continue;
                }

                Console.Clear();
                Console.WriteLine(@"Podaj swoje imię:");
                var name = Console.ReadLine()?? "";

                Console.Clear();
                Console.WriteLine(@"Podaj swój adres:");
                var address = Console.ReadLine() ?? "";

                Console.Clear();
                _ordersCenter.Order(new Client(name, address, location));

                Console.ReadLine();
                Console.Clear();

            }

        }

        private void CreateRestaurants()
        {
            _ordersCenter.Register(new Restaurant("Dolnośląskie", 1, true));
            _ordersCenter.Register(new Restaurant("Kujawsko-pomorskie", 2, true));
            _ordersCenter.Register(new Restaurant("Lubelskie", 3, true));
            _ordersCenter.Register(new Restaurant("Lubuskie", 4, true));
            _ordersCenter.Register(new Restaurant("Łódzkie", 5, true));
            _ordersCenter.Register(new Restaurant("Małopolskie", 6, true));
            _ordersCenter.Register(new Restaurant("Mazowieckie", 7, true));
            _ordersCenter.Register(new Restaurant("Opolskie", 8, true));
            _ordersCenter.Register(new Restaurant("Podkarpackie", 9, true));
            _ordersCenter.Register(new Restaurant("Podlaskiee", 10, true));
            _ordersCenter.Register(new Restaurant("Pomorskie", 11, true));
            _ordersCenter.Register(new Restaurant("Śląskie", 12, true));
            _ordersCenter.Register(new Restaurant("Świętokrzyskie", 13, true));
            _ordersCenter.Register(new Restaurant("Warmińsko-mazurskie", 14, true));
            _ordersCenter.Register(new Restaurant("Wielkopolskie", 15, true));
            _ordersCenter.Register(new Restaurant("Zachodniopomorskie", 16, true));
        }
    }
}
