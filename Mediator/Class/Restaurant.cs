﻿using Mediator.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mediator.Class
{
    public class Restaurant : IRestaurant
    {
        private string _name;
        private int _location;
        private bool _isOpen;
        public string Name => _name;
        public int Location => _location;
        public bool IsOpen => _isOpen;

        public Restaurant(string name, int location, bool isOpen)
        {
            _name = name;
            _location = location;
            _isOpen = isOpen;
        }

        public void MakeOrder(IClient client)
        {
            Console.WriteLine($"Restauracja komunikat: klient: {client.Name} złożył zamówienie na adres: {client.Address}");
        }
    }
}
