﻿using Mediator.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mediator.Class
{
    public class Client : IClient
    {
        private string _name;
        private string _address;
        private int _location;
        public string Name => _name;
        public string Address => _address;
        public int Location => _location;

        public Client(string name, string address, int location)
        {
            _name = name;
            _address = address;
            _location = location;
        }
        public void Acknowledge(string name)
        {
            Console.WriteLine($"Klient powiadomienie: restauracja z województwa {name} przyjeła zamówienie");
        }
    }
}
