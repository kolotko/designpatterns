﻿using Mediator.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mediator.Class
{
    public class OrdersCenter : IOrdersCenter
    {
        private readonly Dictionary<string, IRestaurant> _restaurants = new Dictionary<string,IRestaurant>();
        public void Order(IClient client)
        {
            foreach (var restaurant in _restaurants.Values.Where(c => c.IsOpen))
            {
                if (restaurant.Location == client.Location)
                {
                    restaurant.MakeOrder(client);
                    client.Acknowledge(restaurant.Name);
                }
            }
        }

        public void Register(IRestaurant restaurant)
        {
            if (!_restaurants.ContainsValue(restaurant))
                _restaurants.Add(restaurant.Name, restaurant);
        }
    }
}
