﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mediator.Interfaces
{
    public interface IRestaurant
    {
        string Name { get; }
        int Location { get; }
        bool IsOpen { get; }
        void MakeOrder(IClient client);
    }
}
