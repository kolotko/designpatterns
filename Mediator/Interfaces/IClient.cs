﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mediator.Interfaces
{
    public interface IClient
    {
        string Name { get; }
        string Address { get; }
        int Location { get; }
        void Acknowledge(string name);
    }
}
