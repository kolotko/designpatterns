﻿using Mediator;
using Mediator.Class;
using Mediator.Interfaces;
using Microsoft.Extensions.DependencyInjection;

var services = ConfigureServices();
var servicesProvider = services.BuildServiceProvider();
servicesProvider.GetService<Application>()?.Run();

static IServiceCollection ConfigureServices()
{
    IServiceCollection services = new ServiceCollection();
    services.AddSingleton<IOrdersCenter>(new OrdersCenter());
    services.AddSingleton<Application>();

    return services;
}
