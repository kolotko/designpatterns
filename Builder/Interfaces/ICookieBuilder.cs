﻿using Builder.Class;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Builder.Interfaces
{
    public interface ICookieBuilder
    {
        public void SetBakingTime();
        public void SetIngredients();
        public void SetPrice();
        public Cookie Build();
    }
}
