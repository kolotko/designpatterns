﻿using Builder.Class;
using Builder.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Builder
{
    public class Application
    {
        public void Run()
        {
            Console.WriteLine(@"
                    Witaj
            ");
            while (true)
            {
                Console.WriteLine(@"
                    Jesteśmy profesjonalną piekarnią pozwalającą na tworzenie i sprzedaż wypieków.
                    Wybierz co chcesz stworzyć:
                    1: ciastko
                    2: tort
                ");

                string userInput = Console.ReadLine() ?? "";
                if (!ValidateUserInput(userInput, new[] { "1", "2" }))
                    continue;

                EProduct product = (EProduct)Enum.Parse(typeof(EProduct), userInput);

                switch (product)
                {
                    case EProduct.Cookie:
                        var cookie = new Cookie();
                        cookie.SetBakingTime();
                        cookie.SetPrice();
                        cookie.Build();
                        break;
                    case EProduct.Cake:
                        new Cake.CakeBuilder().SetPrice().SetBakingTime().SetIngredients().Build();
                        break;
                }
            }
        }

        private bool ValidateUserInput(string userInput, string[] validValues)
        {
            if (String.IsNullOrEmpty(userInput) || !validValues.Contains(userInput))
            {
                Console.Clear();
                Console.WriteLine(@"
                    Wprowadzono niepoprawną wartość, spróbuj jeszcze raz.
                    ");
                return false;
            }
            return true;
        }
    }
}
