﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Builder.Enums
{
    public enum EIngredients
    {
        Strawberry = 1,
        blueberry = 2,
        raspberry = 3,
        Cherry = 4
    }
}
