﻿using Builder.Enums;
using Builder.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Builder.Class
{
    //Fluent interface
    public class Cookie : ICookieBuilder
    {
        private int _bakingTime;
        private List<string> _ingredients = new List<string>();
        private int _price;

        public int BakingTime => _bakingTime;
        public List<string> Ingredients => _ingredients;
        public int Price => _price;
        public void SetBakingTime()
        {
            Console.Clear();
            while (true)
            {
                Console.WriteLine(@"
                    Wprowadź czas pieczenia w minutach:
                ");

                var userInput = Console.ReadLine() ?? "";
                if (int.TryParse(userInput, out _bakingTime))
                    return;

                Console.Clear();
                Console.WriteLine(@"
                    Wprowadzono niepoprawną wartość, spróbuj jeszcze raz.
                ");       
            }
        }

        public void SetIngredients()
        {
            _ingredients = new List<string>();
            while (_ingredients.Count < 2)
            {
                Console.Clear();
                Console.WriteLine(@"
                    Wybierz 2 składniki których chcesz użyć:
                    1: truskawka
                    2: jagoda
                    3: malina
                    4: wiśnia
                ");

                string userInput = Console.ReadLine() ?? "";
                if (!ValidateUserInput(userInput, new[] { "1", "2", "3", "4" }))
                    continue;

                EIngredients ingredients = (EIngredients)Enum.Parse(typeof(EIngredients), userInput);

                switch (ingredients)
                {
                    case EIngredients.Strawberry:
                        _ingredients.Add("truskawkę");
                        break;
                    case EIngredients.blueberry:
                        _ingredients.Add("jagodę");
                        break;
                    case EIngredients.raspberry:
                        _ingredients.Add("malinę");
                        break;
                    case EIngredients.Cherry:
                        _ingredients.Add("wiśnię");
                        break;
                }
            }
            return;
        }

        public void SetPrice()
        {
            Console.Clear();
            while (true)
            {
                Console.WriteLine(@"
                    Wprowadź cene za wypiek:
                ");

                string userInput = Console.ReadLine() ?? "";
                if (int.TryParse(userInput, out _price))
                    return;

                Console.Clear();
                Console.WriteLine(@"
                    Wprowadzono niepoprawną wartość, spróbuj jeszcze raz.
                    ");
            }
        }

        public Cookie Build()
        {
            Console.Clear();
            Console.WriteLine("Rozpoczynamy proces peiczenia");
            Thread.Sleep(1000);
            Console.WriteLine("Do ciasta dodajemy następujące składniki: ");
            Thread.Sleep(1000);
            foreach (var ingredient in _ingredients)
            {
                Console.WriteLine($"- {ingredient}");
                Thread.Sleep(1000);
            }

            Console.WriteLine($"Wymieszane składniki pieczemy przez: {_bakingTime}");
            Thread.Sleep(1000);
            Console.WriteLine($"Nasza babeczkę można kupić za: {_price}");

            return this;
        }

        private bool ValidateUserInput(string userInput, string[] validValues)
        {
            if (String.IsNullOrEmpty(userInput) || !validValues.Contains(userInput))
            {
                Console.Clear();
                Console.WriteLine(@"
                    Wprowadzono niepoprawną wartość, spróbuj jeszcze raz.
                    ");
                return false;
            }
            return true;
        }
    }
}
