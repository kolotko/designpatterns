﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proxy.CustomExceptions
{
    public class AdditionException : Exception
    {
        public AdditionException(string message) : base(message) { }
    }
}
