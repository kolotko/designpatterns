﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proxy.CustomExceptions
{
    public class MultiplicationException : Exception
    {
        public MultiplicationException(string message) : base(message) { }
    }
}
