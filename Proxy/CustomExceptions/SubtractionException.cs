﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proxy.CustomExceptions
{
    public class SubtractionException : Exception
    {
        public SubtractionException(string message) : base(message) { }
    }
}
