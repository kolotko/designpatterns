﻿using Proxy.Enums;
using Proxy.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proxy
{
    public class Application
    {
        private readonly IAddition _addition;
        private readonly IMultiplication _multiplication;
        private readonly ISubtraction _subtraction;

        public Application(IAddition addition, IMultiplication multiplication, ISubtraction subtraction)
        {
            _addition = addition;
            _multiplication = multiplication;
            _subtraction = subtraction;
        }
        public void Run()
        {
            Console.WriteLine(@"
                    Witaj
            ");
            while (true)
            {
                EMathematicalOperation mathematicalOperation = GetMathematicalOperationFromUser();

                int[] userNumbers = new int[2];
                for (int i = 0; i < 2; i++)
                {
                    Console.Clear();
                    userNumbers[i] = GetNumberFromUser(i);
                }

                try
                {
                    Console.Clear();
                    int result = 0;
                    switch (mathematicalOperation)
                    {
                        case EMathematicalOperation.Addition:
                            result = _addition.Calculate(userNumbers[0], userNumbers[1]);
                            Console.WriteLine(result.ToString());
                            break;
                        case EMathematicalOperation.Subtraction:
                            result = _subtraction.Calculate(userNumbers[0], userNumbers[1]);
                            Console.WriteLine(result.ToString());
                            break;
                        case EMathematicalOperation.Multiplication:
                            result = _multiplication.Calculate(userNumbers[0], userNumbers[1]);
                            Console.WriteLine(result.ToString());
                            break;
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }

                Console.WriteLine("Naciśnij dowolny klawisz aby kontynuować");
                Console.ReadLine();
                Console.Clear();
            }
        }

        private EMathematicalOperation GetMathematicalOperationFromUser()
        {
            Console.Clear();
            while (true)
            {
                Console.WriteLine(@"
                    Nasz prosty  kalkulator może dodawać, odejmować oraz mnożyć. 
                    Posiada jednak kilka ograniczeń: 
                    - dodawać można tylko liczby mniejsze lub równe 100
                    - odejmować można tylko liczbę mniejszą od większej
                    - mnożyć można liczby mniejsze lub równe 50

                    Wybierz które działanie chcesz wykonać:
                    1: Dodawanie
                    2: Odejmowanie
                    3: Mnożenie
                ");

                string userInput = Console.ReadLine() ?? "";
                if (!ValidateUserInput(userInput, new[] { "1", "2", "3" }))
                    continue;

                return (EMathematicalOperation)Enum.Parse(typeof(EMathematicalOperation), userInput);
            }
        }

        private int GetNumberFromUser(int iterator)
        {
            while (true)
            {
                Console.WriteLine(@$"
                    Podaj liczbę {iterator};
                ");

                string userInput = Console.ReadLine() ?? "";
                int userValue;
                int.TryParse(userInput, out userValue);

                if (userValue == 0)
                {
                    Console.Clear();
                    Console.WriteLine(@"
                    Wprowadzono niepoprawną wartość, spróbuj jeszcze raz.
                    ");
                    continue;
                }
                    

                return userValue;
            }
        }

        private bool ValidateUserInput(string userInput, string[] validValues)
        {
            if (String.IsNullOrEmpty(userInput) || !validValues.Contains(userInput))
            {
                Console.Clear();
                Console.WriteLine(@"
                    Wprowadzono niepoprawną wartość, spróbuj jeszcze raz.
                    ");
                return false;
            }
            return true;
        }
    }
}
