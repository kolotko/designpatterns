﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Proxy;
using Proxy.Class;
using Proxy.Interfaces;
using Proxy.Proxy;

var services = ConfigureServices();
var servicesProvider = services.BuildServiceProvider();
servicesProvider.GetService<Application>()?.Run();

/// <summary>
/// Register services
/// </summary>
static IServiceCollection ConfigureServices()
{
    IServiceCollection services = new ServiceCollection();

    services.AddSingleton<IAddition>(new AdditionProxy(new Addition()));

    services.AddSingleton<IMultiplication>(new MultiplicationProxy(new Multiplication()));

    services.AddSingleton<ISubtraction>(new SubtractionProxy(new Subtraction()));

    services.AddSingleton<Application>();

    return services;
}