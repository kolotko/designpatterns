﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proxy.Enums
{
    public enum EMathematicalOperation
    {
        Addition = 1,
        Subtraction = 2,
        Multiplication = 3
    }
}
