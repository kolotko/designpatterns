﻿using Proxy.CustomExceptions;
using Proxy.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proxy.Proxy
{
    public class AdditionProxy : IAddition
    {
        private readonly IAddition _addition;

        public AdditionProxy(IAddition addition)
        {
            _addition = addition;
        }

        public int Calculate(int a, int b)
        {
            if (a >= 100 || b >= 100)
                throw new AdditionException("Nie możesz dodać liczb większych niż 100");

            return _addition.Calculate(a, b);
        }
    }
}
