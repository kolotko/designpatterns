﻿using Proxy.CustomExceptions;
using Proxy.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proxy.Proxy
{
    public class MultiplicationProxy : IMultiplication
    {
        private readonly IMultiplication _multiplication;

        public MultiplicationProxy(IMultiplication multiplication)
        {
            _multiplication = multiplication;
        }

        public int Calculate(int a, int b)
        {
            if (a >= 50 || b >= 100)
                throw new MultiplicationException("Nie możesz mnożyć liczb większych niż 50");

            return _multiplication.Calculate(a, b);
        }
    }
}
