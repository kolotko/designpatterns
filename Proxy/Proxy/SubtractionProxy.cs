﻿using Proxy.CustomExceptions;
using Proxy.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proxy.Proxy
{
    public class SubtractionProxy : ISubtraction
    {
        private readonly ISubtraction _subtraction;

        public SubtractionProxy(ISubtraction subtraction)
        {
            _subtraction = subtraction;
        }

        public int Calculate(int a, int b)
        {
            if (a < b)
                throw new SubtractionException("Nie możesz odjąć większej liczby od mniejszej");

            return _subtraction.Calculate(a, b);
        }
    }
}
