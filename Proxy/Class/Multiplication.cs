﻿using Proxy.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proxy.Class
{
    public class Multiplication : IMultiplication
    {
        public int Calculate(int a, int b)
        {
            return a * b;
        }
    }
}
