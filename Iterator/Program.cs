﻿using Iterator;
using Iterator.Class;
using Iterator.Interfaces;
using Microsoft.Extensions.DependencyInjection;

var services = ConfigureServices();
var servicesProvider = services.BuildServiceProvider();
servicesProvider.GetService<Application>()?.Run();

static IServiceCollection ConfigureServices()
{
    IServiceCollection services = new ServiceCollection();
    services.AddSingleton<Application>();
    services.AddSingleton<IWordsCollection>(new WordsCollection());

    return services;
}