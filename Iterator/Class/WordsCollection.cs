﻿using Iterator.Interfaces;
using System.Collections;

namespace Iterator.Class
{
    internal class WordsCollection : IWordsCollection
    {
        private List<string> _collection = new List<string>();
        private bool _direction = false;

        public List<string> Collection => _collection;

        public bool Direction => _direction;

        public void ReverseDirection()
        {
            _direction = !_direction;
        }
        public List<string> getItems()
        {
            return _collection;
        }
        public void AddItem(string item)
        {
            this._collection.Add(item);
        }
        public void Clear()
        {
            _collection = new List<string>();
        }
        public IEnumerator GetEnumerator()
        {
            return new AlphabeticalOrderIterator(this, _direction);
        }
    }
}
