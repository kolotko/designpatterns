﻿using Iterator.Interfaces;
using System.Collections;

namespace Iterator.Class
{
    internal class AlphabeticalOrderIterator : IEnumerator
    {
        private IWordsCollection _collection;
        private int _position = -1;
        private bool _reverse = false;
        public AlphabeticalOrderIterator(IWordsCollection collection, bool reverse = false)
        {
            _collection = collection;
            this._reverse = reverse;

            if (reverse)
            {
                this._position = collection.getItems().Count;
            }
        }
        public object Current => this._collection.getItems()[_position];

        public bool MoveNext()
        {
            int updatedPosition = this._position + (this._reverse ? -1 : 1);
            if (updatedPosition >= 0 && updatedPosition < this._collection.getItems().Count)
            {
                this._position = updatedPosition;
                return true;
            }
            else
            {
                return false;
            }
        }

        public void Reset()
        {
            this._position = this._reverse ? this._collection.getItems().Count - 1 : 0;
        }
    }
}
