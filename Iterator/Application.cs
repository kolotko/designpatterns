﻿using Iterator.Class;
using Iterator.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Iterator
{
    internal class Application
    {
        private IWordsCollection _collection;
        private int _count;
        public Application(IWordsCollection collection)
        {
            _collection = collection;
            _count = 1;
        }
        
        public void Run()
        {
            Console.WriteLine("Witaj");
            while (true)
            {
                if (_count < 6)
                {
                    Console.WriteLine("Wprowadź 5 dowolnych słów:");
                    Console.WriteLine($"Słowo numer {_count}");
                    var userInput = Console.ReadLine() ?? "";
                    _collection.AddItem(userInput);
                    _count++;
                    Console.Clear();
                    continue;
                }

                foreach (var element in _collection)
                {
                    Console.WriteLine(element);
                }

                Console.WriteLine("Odwrócenie kolejności:");

                _collection.ReverseDirection();

                foreach (var element in _collection)
                {
                    Console.WriteLine(element);
                }

                Console.ReadLine();
                Console.Clear();
                _count = 1;
                _collection.Clear();
            }
        }
    }
}
