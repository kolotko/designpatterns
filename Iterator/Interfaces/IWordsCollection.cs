﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Iterator.Interfaces
{
    internal interface IWordsCollection : IEnumerable
    {
        List<string> Collection { get; }
        bool Direction { get; }

        void ReverseDirection();
        List<string> getItems();
        void AddItem(string item);
        void Clear();
    }
}
