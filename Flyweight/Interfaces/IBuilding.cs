﻿using Flyweight.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flyweight.Interfaces
{
    public interface IBuilding
    {
        public EBuilding BuildingKind { get; set; }
        public void SetLocation(double longitude, double latitude);
    }
}
