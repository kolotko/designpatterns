﻿using Microsoft.Extensions.DependencyInjection;
using Flyweight;
using Flyweight.Class;

var services = ConfigureServices();
var servicesProvider = services.BuildServiceProvider();
servicesProvider.GetService<Application>()?.Run();

/// <summary>
/// Register services
/// </summary>
static IServiceCollection ConfigureServices()
{
    IServiceCollection services = new ServiceCollection();
    services.AddSingleton(new BuildingFactory());
    services.AddSingleton<Application>();

    return services;
}