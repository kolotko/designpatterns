﻿using Flyweight.Class;
using Flyweight.Enums;
using Flyweight.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flyweight
{
    public class Application
    {
        private BuildingFactory _buildingFactory;
        private List<int> _userNumber;
        private List<BuildingManager> _userBuildings;
        private bool _isFlyweight;

        public Application(BuildingFactory buildingFactory)
        {
            _buildingFactory = buildingFactory;
            _isFlyweight = true;
        }

        public void Run()
        {
            while (true)
            {
                _userNumber = new List<int>();
                _userBuildings = new List<BuildingManager>();
                Console.WriteLine($"Witaj. Jesteś właścicielem wielu domów oraz blokw mieszkalnych");
                GetNumberOfBuilding();
                Random rng = new Random();
                for (int e = 0; e < _userNumber.Count; e++)
                {

                    for (int i = 0; i < _userNumber[e]; i++)
                    {
                        var newBuilding = new BuildingManager(_buildingFactory, (EBuilding)e, _isFlyweight);
                        newBuilding.SetLocation(rng.NextDouble(), rng.NextDouble());
                        _userBuildings.Add(newBuilding);
                    }
                }

                Console.WriteLine($"W sumie posiadasz {_userBuildings.Count} budynków");
                Console.ReadLine();
                Console.Clear();

            }
        }

        private void GetNumberOfBuilding()
        {
            string buildingName = "bloków mieszkalnych";
            while (true)
            {
                Console.WriteLine($"Wprowadź ilosć posiadanych przez Ciebie {buildingName}: ");
                var userInput = Console.ReadLine() ?? "";

                int userNumber;
                if (!int.TryParse(userInput, out userNumber))
                {
                    Console.Clear();
                    Console.WriteLine("Wprowadzono niepoprawną wartość, spróbuj jeszcze raz.");
                    continue;
                }

                Console.Clear();
                _userNumber.Add(userNumber);
                if (buildingName != "domów")
                {
                    buildingName = "domów";
                    continue;
                }

                return;
            }
                
        }
    }
}
