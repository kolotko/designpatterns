﻿using Flyweight.Enums;
using Flyweight.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flyweight.Class
{
    public class ApartmentBuilding : IBuilding
    {
        public EBuilding BuildingKind { get; set; }

        public void SetLocation(double longitude, double latitude)
        {
            Console.WriteLine($"Blok mieszkalny znajduje się {longitude} stopniach długości i {latitude} stopniach szerokości");
        }
    }
}
