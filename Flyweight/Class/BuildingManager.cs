﻿using Flyweight.Enums;
using Flyweight.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flyweight.Class
{
    public class BuildingManager
    {
        private double _latitude = 0;
        private double _longitude = 0;
        private IBuilding _building;

        public BuildingManager(BuildingFactory buildingFactory, EBuilding buildingType, bool isFlyweight)
        {
            _building = buildingFactory.GetBuilding(buildingType, isFlyweight);
        }

        public void SetLocation(double latitude, double longitude)
        {
            _latitude = latitude; 
            _longitude = longitude;
            _building.SetLocation(_latitude, _longitude);
        }
    }
}
