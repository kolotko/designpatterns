﻿using Flyweight.Enums;
using Flyweight.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flyweight.Class
{
    public class BuildingFactory
    {
        private readonly IDictionary<EBuilding, IBuilding> _building = new Dictionary<EBuilding, IBuilding>();

        public IBuilding GetBuilding(EBuilding buildingType, bool isFlyweight)
        {
            if (isFlyweight)
            {
                if (_building.ContainsKey(buildingType))
                    return _building[buildingType];

                var newBuilding = CreateBuilding(buildingType);
                _building.Add(buildingType, newBuilding);
                return newBuilding;
            }
            else
            {
                var newBuilding = CreateBuilding(buildingType);
                return newBuilding;
            }
        }

        private IBuilding CreateBuilding(EBuilding buildingType) =>
            buildingType switch
            {
                EBuilding.House => new House(),
                EBuilding.ApartmentBuilding => new ApartmentBuilding(),
                _ => throw new NotImplementedException()
            };
    }
}
