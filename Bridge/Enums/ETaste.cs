﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridge.Enums
{
    internal enum ETaste
    {
        Orange = 1,
        Apple = 2
    }
}
