﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridge.Enums
{
    internal enum EDrink
    {
        Juice = 1,
        Water = 2
    }
}
