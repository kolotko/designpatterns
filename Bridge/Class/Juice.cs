﻿using Bridge.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridge.Class
{
    internal class Juice : IDrink
    {
        protected ITaste _taste;

        public Juice(ITaste taste)
        {
            _taste = taste;
        }

        public void TryIt()
        {
            Console.WriteLine($"Sok o smaku: {_taste.SetTast()}");
        }
    }
}
