﻿using Bridge.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridge.Class
{
    internal class Water : IDrink
    {
        protected ITaste _taste;

        public Water(ITaste taste)
        {
            _taste = taste;
        }

        public void TryIt()
        {
            Console.WriteLine($"Woda o smaku: {_taste.SetTast()}");
        }
    }
}
