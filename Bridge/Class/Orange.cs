﻿using Bridge.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridge.Class
{
    internal class Orange : ITaste
    {
        public string SetTast()
        {
            return "pomarańczy";
        }
    }
}
