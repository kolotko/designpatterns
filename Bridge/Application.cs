﻿using Bridge.Class;
using Bridge.Enums;
using Bridge.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bridge
{
    internal class Application
    {
        private ETaste _eTaste = ETaste.Orange;
        private EDrink _eDrink = EDrink.Water;
        public void Run()
        {
            Console.WriteLine("Witaj");
            while (true)
            {
                Console.WriteLine();
                GetDrink();
                Console.WriteLine();
                GetTaste();

                IDrink? drink;
                switch ((_eDrink, _eTaste))
                {
                    case (EDrink.Water, ETaste.Orange):
                        drink = new Water(new Orange());
                        break;
                    case (EDrink.Water, ETaste.Apple):
                        drink = new Water(new Apple());
                        break;
                    case (EDrink.Juice, ETaste.Orange):
                        drink = new Juice(new Orange());
                        break;
                    case (EDrink.Juice, ETaste.Apple):
                        drink = new Juice(new Apple());
                        break;
                    default:
                        drink = null;
                        break;
                }

                drink?.TryIt();
                Console.ReadLine();
                Console.Clear();
            }
        }

        private void GetDrink()
        {
            while (true)
            {
                Console.WriteLine("Wybierz napój który chesz spróbować:");
                Console.WriteLine("1: sok");
                Console.WriteLine("2: woda");

                string userInput = Console.ReadLine() ?? "";
                if (!ValidateUserInput(userInput, new[] { "1", "2" }))
                    continue;

                _eDrink = (EDrink)Enum.Parse(typeof(EDrink), userInput);
               

                return;
            }
        }

        private void GetTaste()
        {
            while (true)
            {
                Console.WriteLine("Wybierz smak który chesz spróbować:");
                Console.WriteLine("1: pomarańcza");
                Console.WriteLine("2: jabłko");

                string userInput = Console.ReadLine() ?? "";
                if (!ValidateUserInput(userInput, new[] { "1", "2" }))
                    continue;

                _eTaste = (ETaste)Enum.Parse(typeof(ETaste), userInput);

                return;
            }
        }

        private bool ValidateUserInput(string userInput, string[] validValues)
        {
            if (String.IsNullOrEmpty(userInput) || !validValues.Contains(userInput))
            {
                Console.Clear();
                Console.WriteLine("Wprowadzono niepoprawną wartość, spróbuj jeszcze raz.");
                return false;
            }
            return true;
        }
    }
}
