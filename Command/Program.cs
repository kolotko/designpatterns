﻿using Command;
using Command.Class;
using Microsoft.Extensions.DependencyInjection;

var services = ConfigureServices();
var servicesProvider = services.BuildServiceProvider();
servicesProvider.GetService<Invoker>()?.Run();

static IServiceCollection ConfigureServices()
{
    IServiceCollection services = new ServiceCollection();
    services.AddSingleton<Invoker>();
    services.AddSingleton(new Pasta(new PastaReceiver()));
    services.AddSingleton(new Sandwich(new SandwichReceiver()));
    services.AddSingleton(new ScrambledEggs(new ScrambledEggsReceiver()));

    return services;
}
