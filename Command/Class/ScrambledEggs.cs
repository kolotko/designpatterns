﻿using Command.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Command.Class
{
    internal class ScrambledEggs : ICommand
    {
        private IReceiver _scrambledEggsReceiver;
        public ScrambledEggs(IReceiver scrambledEggsReceiver)
        {
            _scrambledEggsReceiver = scrambledEggsReceiver;
        }
        public void Execute()
        {
            _scrambledEggsReceiver.CreateDish();
        }
    }
}
