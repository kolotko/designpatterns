﻿using Command.Interfaces;

namespace Command.Class
{
    internal class Pasta : ICommand
    {
        private IReceiver _pastaReceiver;

        public Pasta(IReceiver pastaReceiver)
        {
            _pastaReceiver = pastaReceiver;
        }
        public void Execute()
        {
            _pastaReceiver.CreateDish();
        }
    }
}
