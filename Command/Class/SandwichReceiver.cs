﻿using Command.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Command.Class
{
    internal class SandwichReceiver : IReceiver
    {
        public void CreateDish()
        {
            Console.WriteLine("Podano kanapkę z żółtym serem oraz pomidorem");
        }
    }
}
