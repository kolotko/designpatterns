﻿using Command.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Command.Class
{
    internal class Sandwich : ICommand
    {
        private IReceiver _sandwichReceiver;
        public Sandwich(IReceiver sandwichReceiver)
        {
            _sandwichReceiver = sandwichReceiver;
        }
        public void Execute()
        {
            _sandwichReceiver.CreateDish();
        }
    }
}
