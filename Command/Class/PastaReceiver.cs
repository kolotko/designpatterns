﻿using Command.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Command.Class
{
    internal class PastaReceiver : IReceiver
    {
        public void CreateDish()
        {
            Console.WriteLine("Podano makaron z kurczakiem, brokułem oraz sosem grzybowym");
        }
    }
}
