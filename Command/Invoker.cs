﻿using Command.Class;
using Command.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Command
{
    internal class Invoker
    {
        private Pasta _pasta;
        private Sandwich _sandwich;
        private ScrambledEggs _scrambledEggs;
        public Invoker(Pasta pasta, Sandwich sandwich, ScrambledEggs scrambledEggs)
        {
            _pasta = pasta;
            _sandwich = sandwich;
            _scrambledEggs = scrambledEggs;
        }

        public void Run()
        {
            Console.WriteLine("Witaj w naszej restauracji.");
            while (true)
            {
                Console.WriteLine("Co mogę podać?");
                Console.WriteLine("1: jajecznica");
                Console.WriteLine("2: kanapka");
                Console.WriteLine("3: makaron");

                string userInput = Console.ReadLine() ?? "";
                if (!ValidateUserInput(userInput, new[] { "1", "2", "3" }))
                    continue;

                Console.Clear();

                EDish dish = (EDish)Enum.Parse(typeof(EDish), userInput);
                switch (dish) 
                {
                    case EDish.ScrambledEggs:
                        _scrambledEggs.Execute();
                        break;
                    case EDish.Sandwich:
                        _sandwich.Execute();
                        break;
                    case EDish.Pasta:
                        _pasta.Execute();
                        break;
                }

                Console.ReadLine();
                Console.Clear();
            }
        }

        private bool ValidateUserInput(string userInput, string[] validValues)
        {
            if (String.IsNullOrEmpty(userInput) || !validValues.Contains(userInput))
            {
                Console.Clear();
                Console.WriteLine(@"
                    Wprowadzono niepoprawną wartość, spróbuj jeszcze raz.
                    ");
                return false;
            }
            return true;
        }
    }
}
