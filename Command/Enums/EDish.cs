﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Command.Enums
{
    internal enum EDish
    {
        ScrambledEggs = 1,
        Sandwich = 2,
        Pasta = 3
    }
}
