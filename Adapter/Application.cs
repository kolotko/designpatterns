﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Adapter.Class;

namespace Adapter
{
    public class Application
    {
        private Person _person = new();
        public void Run()
        {
            while (true)
            {
                if (string.IsNullOrEmpty(_person.Name))
                {
                    Console.WriteLine(@"
                        Wpisz swoje imię:
                    ");

                    string userInput = Console.ReadLine() ?? "";
                    if (!CheckThatStringContainLetter(userInput))
                        continue;

                    _person.Name = userInput;
                    Console.Clear();
                }

                if (string.IsNullOrEmpty(_person.Surname))
                {
                    Console.WriteLine(@"
                        Wpisz swoje nazwisko:
                    ");

                    string userInput = Console.ReadLine() ?? "";
                    if (!CheckThatStringContainLetter(userInput))
                        continue;

                    _person.Surname = userInput;
                    Console.Clear();
                }

                if (_person.Age == 0)
                {
                    Console.WriteLine(@"
                        Wpisz swój wiek:
                    ");

                    string userInput = Console.ReadLine() ?? "";
                    if (!CheckThatStringContainNumber(userInput))
                        continue;

                    _person.Age = Int32.Parse(userInput);
                    Console.Clear();
                }

                var stringPerson = new PersonAsStringAdapter(_person);
                var jsonPerson = new PersonAsJSONAdapter(_person);
                var xmlPerson = new PersonAsXMLAdapter(_person);
                Console.WriteLine("Format 1");
                Console.WriteLine(stringPerson.Get());
                Console.WriteLine("Format 2");
                Console.WriteLine(jsonPerson.Get());
                Console.WriteLine("Format 3");
                Console.WriteLine(xmlPerson.Get());

                Console.ReadLine();
                _person = new();
                Console.Clear();
            }
        }

        private bool CheckThatStringContainLetter(string userInput)
        {
            if (userInput.Any(s => !char.IsLetter(s)))
            {
                Console.Clear();
                Console.WriteLine(@"
                    Wprowadzono niepoprawną wartość, spróbuj jeszcze raz.
                    ");
                return false;
            }
            return true;
        }

        private bool CheckThatStringContainNumber(string userInput)
        {
            if (userInput.Any(s => !char.IsNumber(s)))
            {
                Console.Clear();
                Console.WriteLine(@"
                    Wprowadzono niepoprawną wartość, spróbuj jeszcze raz.
                    ");
                return false;
            }
            return true;
        }
    }
}
