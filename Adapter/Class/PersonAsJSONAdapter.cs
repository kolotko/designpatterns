﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Adapter.Interfaces;

namespace Adapter.Class
{
    public class PersonAsJSONAdapter : IAdapter
    {
        private Person _person;
        public PersonAsJSONAdapter(Person person)
        {
            _person = person;
        }

        public string Get()
        {
            return @$"
            {{
                Imię: {_person.Name},
                Nazwisko: {_person.Surname},
                Wiek: {_person.Age.ToString()}
            }}";
        }
    }
}
