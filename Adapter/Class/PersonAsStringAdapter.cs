﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Adapter.Interfaces;

namespace Adapter.Class
{
    public class PersonAsStringAdapter : IAdapter
    {
        private Person _person;
        public PersonAsStringAdapter(Person person)
        {
            _person = person;
        }

        public string Get()
        {
            return $"Imię: {_person.Name}, Nazwisko: {_person.Surname}, Wiek: {_person.Age.ToString()}";
        }
    }
}
