﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Adapter.Interfaces;

namespace Adapter.Class
{
    public class PersonAsXMLAdapter : IAdapter
    {
        private Person _person;
        public PersonAsXMLAdapter(Person person)
        {
            _person = person;
        }

        public string Get()
        {
            return @$"
            <person>
                <imie>{_person.Name}</imie>
                <nazwisko>{_person.Surname}</nazwisko>
                <wiek>{_person.Age.ToString()}</wiek>
            </person>";
        }
    }
}
